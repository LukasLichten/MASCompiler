﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASCompiler
{
    public class MASConfig
    {
        public string MasLocation { get; set; }

        public string[] rules { get; set; }

        public string[] getFileLocations(string origin)
        {
            HashSet<string> files = new HashSet<string>();
            for (int i = 0; i < rules.Length; i++)
            {

                if (rules[i].StartsWith("!"))
                {
                    if (rules[i].Replace("!", "").Trim().StartsWith("*"))
                    {
                        string extension = rules[i].Replace("!", "").Replace("*", "").Replace(".", "").Trim().ToLower();
                        HashSet<string> reducedList = new HashSet<string>();
                        foreach (string f in files)
                        {
                            if (Path.GetExtension(f).Replace(".", "").ToLower() != extension)
                                reducedList.Add(f);
                        }
                        files = reducedList;
                    }
                    else
                    {
                        //It is a directory
                        HashSet<string> set = getFilesForRule(origin, rules[i]);
                        foreach (string f in set)
                            files.Remove(f);
                    }
                }
                else
                {
                    HashSet<string> set = getFilesForRule(origin, rules[i]);
                    foreach (string f in set)
                        files.Add(f);
                }
            }

            string[] arr = new string[files.Count];

            int iter = 0;

            foreach (string f in files)
            {
                arr[iter] = f;
                iter++;
            }

            return arr;
        }

        private HashSet<string> getFilesForRule(string origin, string rule)
        {
            HashSet<string> set = new HashSet<string>();

            if (rule.StartsWith("!"))
            {
                if (rule.Length > 1)
                    rule = rule.Substring(1, rule.Length).Trim();
                else
                    rule = "";
            }


            if (rule.StartsWith("/") && !rule.Contains("."))
            {
                //This is a directory
                bool recursiv = false;
                if (rule.EndsWith("*"))
                {
                    recursiv = true;
                    rule = rule.Substring(0, rule.Length - 1);
                }
                if (rule.StartsWith("/"))
                    rule = rule.Substring(1, rule.Length - 1);
                if (rule.EndsWith("/"))
                    rule = rule.Substring(0, rule.Length - 1);

                if (!Util.checkForInvalidChars(rule))
                    return new HashSet<string>();

                string path = Path.Combine(origin, rule);
                path = Path.GetFullPath(path);

                if (!Directory.Exists(path))
                    return new HashSet<string>(); //Returning an empty set



                try
                {
                    string[] files = Directory.GetFiles(path);
                    foreach (string f in files)
                    {
                        if (Path.GetExtension(f).ToLower().Replace(".", "") != "mas")
                            set.Add(f);
                    }

                    if (recursiv)
                    {
                        //A star at the end means all directories, so they are added recursivly
                        string[] dirs = Directory.GetDirectories(path);
                        foreach (string d in dirs)
                        {
                            HashSet<string> sub = getFilesForRule(path, "/" + Path.GetFileName(d) + "/*");

                            foreach (string f in sub)
                                set.Add(f);
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    return new HashSet<string>(); //Returning an empty set
                }
            }
            else if (rule.StartsWith("/"))
            {
                if (rule.StartsWith("/"))
                    rule = rule.Substring(1, rule.Length - 1);
                if (rule.EndsWith("/"))
                    rule = rule.Substring(0, rule.Length - 1);

                if (!Util.checkForInvalidChars(rule) || rule.Contains('*'))
                    return new HashSet<string>();

                string path = Path.Combine(origin, rule);
                path = Path.GetFullPath(path);
                if (File.Exists(path))
                {
                    set.Add(path);
                }
            }

            return set;
        }
    }
}
