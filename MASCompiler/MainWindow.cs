﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MASCompiler
{
    public partial class MainWindow : Form
    {

        public RfBuild rfBuild { get; private set; }

        private Dictionary<string, int> originConverter = new Dictionary<string, int>();
        private Dictionary<string, int> categoryConverter = new Dictionary<string, int>();
        private Dictionary<string, int> typeConverter = new Dictionary<string, int>();
        private Dictionary<string, string[]> InstalledMods { get; set; }
        public string Rf2Loc { get; set; }

        public MainWindow()
        {
            InstalledMods = new Dictionary<string, string[]>();
            InitializeComponent();

            rfBuild = new RfBuild();

            originConverter.Add("", 0);
            originConverter.Add("Unknown", 0);
            originConverter.Add("Scratch", 1);
            originConverter.Add("Conversion", 2);
            originConverter.Add("Compsite", 3);

            categoryConverter.Add("", 0);
            categoryConverter.Add("Unknown", 0);

            categoryConverter.Add("Dirt Oval Car", 1);
            categoryConverter.Add("Dirt Rally", 2);
            categoryConverter.Add("GT", 3);
            categoryConverter.Add("Karts", 4);
            categoryConverter.Add("Novel", 5);
            categoryConverter.Add("Open Wheeler", 6);
            categoryConverter.Add("Prototype", 7);
            categoryConverter.Add("Stock Car", 8);
            categoryConverter.Add("Street Car", 9);
            categoryConverter.Add("Touring Car", 10);

            categoryConverter.Add("Dirt Oval", 50);
            categoryConverter.Add("Dirt Track", 51);
            categoryConverter.Add("Kart Track", 52);
            categoryConverter.Add("Novel Track", 53);
            categoryConverter.Add("Oval", 54);
            categoryConverter.Add("Permanent", 55);
            categoryConverter.Add("Rally Cross", 56);
            categoryConverter.Add("Temporary", 57);

            typeConverter.Add("Unknown", 0);
            typeConverter.Add("Location", 1);
            typeConverter.Add("Vehicle", 2);
            typeConverter.Add("Sounds", 3);
            typeConverter.Add("HUD", 4);
            typeConverter.Add("Scripts", 5);
            typeConverter.Add("Commetary", 6);
            typeConverter.Add("Talent", 7);
            typeConverter.Add("Helmets", 8);
            typeConverter.Add("Nations", 9);
            typeConverter.Add("Showroom", 10);
            typeConverter.Add("UI", 11);
            typeConverter.Add("Other", 12);
            typeConverter.Add("", 12);

            string[] typeArr = { "Location", "Vehicle", "Sounds", "HUD", "Scripts", "Commetary", "Talent", "Helmets", "Nations", "Showroom", "UI", "Other" };
            cbType.Items.AddRange(typeArr);
            string[] categoryArr = { "Unknown" };
            cbCategory.Items.AddRange(categoryArr);
            string[] originArr = { "Scratch", "Conversion", "Compsite" };
            cbOrigin.Items.AddRange(originArr);

            string origin = Path.GetDirectoryName(Application.ExecutablePath);
            tbWorkingDirectory.Text = origin;
            tbBuildPath.Text = "/bin/";

            Rf2Loc = Util.getRFactor2Loc(origin);

            TsmiIsSkinpack.CheckedChanged += TsmiIsSkinpack_CheckedChanged;
            TsmiIsSkinpack_CheckedChanged(null, null);

            PopulateRf2Install();

            CbSkinpackName.TextChanged += CbSkinpackName_TextChanged;
        }

        

        private void PopulateRf2Install()
        {
            CbSkinpackName.Items.Clear();
            CbBaseVersion.Items.Clear();
            InstalledMods.Clear();

            List<string> dirs = new List<string>();

            dirs.AddRange(Directory.GetDirectories(Path.Combine(Rf2Loc, "Installed", "Vehicles")));
            dirs.AddRange(Directory.GetDirectories(Path.Combine(Rf2Loc, "Installed", "Locations")));

            foreach (var item in dirs)
            {
                string[] ver = Directory.GetDirectories(item);

                for (int i = 0; i < ver.Length; i++)
                {
                    ver[i] = Path.GetFileName(ver[i]);
                }

                string ele = Path.GetFileName(item);

                InstalledMods.Add(ele, ver);
                CbSkinpackName.Items.Add(ele);
            }
        }

        private void TsmiIsSkinpack_CheckedChanged(object sender, EventArgs e)
        {
            GbSkinpack.Visible = TsmiIsSkinpack.Checked;
            GbSkinpack.Enabled = TsmiIsSkinpack.Checked;
            tbName.Enabled = !TsmiIsSkinpack.Checked;

            Color col = Color.Transparent;

            if (TsmiIsSkinpack.Checked)
                col = Color.LightSteelBlue;

            TsmiIsSkinpack.BackColor = col;
        }

        private void tsmiOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Rf-Build |*.rfbuild";
            dialog.FilterIndex = 0;
            dialog.Title = "Open a Rf-build file";
            dialog.Multiselect = false;

            DialogResult result = dialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            RfBuild[] builds = new RfBuild[0];

            try
            {
                ConfigReader reader = new ConfigReader(dialog.FileName);
                builds = reader.read();
            }
            catch
            {

            }
            if (builds.Length < 1)
            {
                MessageBox.Show("You selected an invalid file!");
                return;
            }

            //TODO: Support for multi build in UI
            RfBuild rfBuild = builds[0];

            openRfBuild(rfBuild, dialog.FileName);
        }

        public void openRfBuild(RfBuild rfBuild, string path)
        {
            this.rfBuild = rfBuild;
            //Filling stuff
            tbWorkingDirectory.Text = Path.GetDirectoryName(path);
            tbFileName.Text = Path.GetFileNameWithoutExtension(path);

            //rfcmp:
            tbName.Text = rfBuild.cmpinfo["name"];
            tbVersion.Text = rfBuild.cmpinfo["version"];
            tbAuthor.Text = rfBuild.cmpinfo["author"];
            tbURL.Text = rfBuild.cmpinfo["url"];
            tbBuildPath.Text = rfBuild.buildPath;

            tbDesc.ResetText();
            string[] desc = rfBuild.cmpinfo["desc"].Split('\u000f');
            foreach (string line in desc)
                tbDesc.AppendText(line.Trim() + "\n");

            while (tbDesc.Text.EndsWith("\n\n\n"))
                tbDesc.Text = tbDesc.Text.Substring(0, tbDesc.Text.Length - 1);

            cbType.Text = typeConverter.FirstOrDefault(x => x.Value == Int32.Parse(rfBuild.cmpinfo["type"])).Key;
            cbType_SelectedIndexChanged(null, null);

            cbOrigin.Text = originConverter.FirstOrDefault(x => x.Value == Int32.Parse(rfBuild.cmpinfo["origin"])).Key;
            cbCategory.Text = categoryConverter.FirstOrDefault(x => x.Value == Int32.Parse(rfBuild.cmpinfo["category"])).Key;

            if (rfBuild.cmpinfo["baseversion"] != "")
            {
                //This is a skinpack

                TsmiIsSkinpack.Checked = true;
                CbSkinpackName.Text = rfBuild.cmpinfo["name"];
                CbBaseVersion.Text = rfBuild.cmpinfo["baseversion"];
            }
            else
            {
                TsmiIsSkinpack.Checked = false;
                CbSkinpackName.Text = "";
                CbBaseVersion.Text = "";
            }

            rebuildList();
        }

        public void rebuildList()
        {
            //MAS files
            listMAS.Items.Clear();

            if (rfBuild.MASConfigs == null)
                return;

            foreach (MASConfig config in rfBuild.MASConfigs)
                listMAS.Items.Add(Path.GetFileNameWithoutExtension(config.MasLocation));
        }

        private void tsmiSave_Click(object sender, EventArgs e)
        {
            fillRfBuild();

            if (!Directory.Exists(tbWorkingDirectory.Text.Trim()))
            {
                MessageBox.Show(tbWorkingDirectory.Text + " is an invalid Path.\nFailed to safe");
                return;
            }

            string file = Path.Combine(tbWorkingDirectory.Text.Trim(), tbFileName.Text.Trim() + ".rfbuild");
            if (File.Exists(file))
            {
                DialogResult result = MessageBox.Show("File " + tbFileName.Text.Trim() + ".rfbuild" + " in " + tbWorkingDirectory.Text.Trim()
                    + " exists already.\nDo you want to Overwrite it?", "Overwrite?", MessageBoxButtons.OKCancel);

                if (result == DialogResult.Cancel)
                    return; //User canncels

                File.Delete(file);
            }

            ConfigWriter writer = new ConfigWriter(file);
            writer.writeRfBuild(rfBuild);
            writer.close();
            MessageBox.Show("Completed Saving");
        }

        public void fillRfBuild()
        {
            //Prep
            if (rfBuild.cmpinfo == null)
                rfBuild.cmpinfo = new Dictionary<string, string>();
            if (rfBuild.MASConfigs == null)
                rfBuild.MASConfigs = new List<MASConfig>();

            string[] keys = { "name", "version", "author", "url", "desc", "type", "origin", "category", "baseversion" }; //TODO: Generalize this, same code in Config Reader
            foreach (string key in keys)
            {
                if (!rfBuild.cmpinfo.ContainsKey(key))
                    rfBuild.cmpinfo.Add(key, "");
            }

            //Filling rfBuild
            rfBuild.buildPath = tbBuildPath.Text.Trim();

            rfBuild.cmpinfo["name"] = tbName.Text.Trim();
            rfBuild.cmpinfo["version"] = tbVersion.Text.Trim();
            rfBuild.cmpinfo["baseversion"] = CbBaseVersion.Text.Trim();
            rfBuild.cmpinfo["author"] = tbAuthor.Text.Trim();
            rfBuild.cmpinfo["url"] = tbURL.Text.Trim();

            string desc = tbDesc.Text.Replace('\n', '\u000f').Trim();

            while (desc.EndsWith("\u000f\u000f"))
                desc = desc.Substring(0, desc.Length - 1);

            if (!desc.EndsWith("\u000f"))
                desc = desc + '\u000f';

            desc = desc.Replace("\r", "");

            rfBuild.cmpinfo["desc"] = desc;
            rfBuild.cmpinfo["type"] = typeConverter[cbType.Text] + "";
            rfBuild.cmpinfo["origin"] = originConverter[cbOrigin.Text] + "";
            rfBuild.cmpinfo["category"] = categoryConverter[cbCategory.Text] + "";
        }

        private void tsmiRun_Click(object sender, EventArgs e)
        {
            string origin = tbWorkingDirectory.Text.Trim();
            if (!Directory.Exists(origin))
            {
                MessageBox.Show("Invalid Working Directory Path, please make sure the folder exists.");
                return;
            }


            string cmppack = Path.Combine(origin, "cmppack.exe");
            Thread thread = new Thread((a0 => Util.extractCmppack(cmppack)));
            thread.Start();

            fillRfBuild();

            


            if (Rf2Loc != null)
                Util.setMAS2PathForCmppack(Rf2Loc);
            else
            {
                thread.Join();
                Util.deleteCmppack(cmppack);
                Console.WriteLine("rf2 folder not found! Please use -rf2=[rf2Loc] to set the rf2 Folder manually!");
                return;
            }

            thread.Join();

            rfBuild.buildMASs(origin, Rf2Loc, false);
            string v = rfBuild.buildRFCMP(origin, cmppack, Rf2Loc, false, null, false);

            thread = new Thread((a0 => Util.deleteCmppack(cmppack)));
            thread.Start();

            if (v != null)
                tbVersion.Text = v;

            thread.Join();

            MessageBox.Show("Build completed!");
        }

        private void btnWorkingDirBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;
            if (tbWorkingDirectory.Text != null && tbWorkingDirectory.Text != "")
                dialog.SelectedPath = tbWorkingDirectory.Text;

            if (DialogResult.OK == dialog.ShowDialog())
            {
                tbWorkingDirectory.Text = dialog.SelectedPath;
            }
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int type = typeConverter[cbType.Text];

            cbCategory.Items.Clear();
            if (type == 1)
            {
                //Loc
                string[] categoryArr = { "Dirt Oval", "Dirt Track", "Kart Track", "Novel Track", "Oval", "Permanent", "Rally Cross", "Temporary" };
                cbCategory.Items.AddRange(categoryArr);
            }
            else if (type == 2)
            {
                //Veh
                string[] categoryArr = { "Dirt Oval Car", "Dirt Rally", "GT", "Karts", "Novel", "Open Wheeler", "Prototype", "Stock Car", "Street Car", "Touring Car" };
                cbCategory.Items.AddRange(categoryArr);
            }
            else
            {
                string[] categoryArr = { "Unknown" };
                cbCategory.Items.AddRange(categoryArr);
            }

        }

        private void btnMasAdd_Click(object sender, EventArgs e)
        {
            MASConfig config = new MASConfig();
            config.MasLocation = "";
            config.rules = new string[0];


            new MasWindow(this, config, tbWorkingDirectory.Text);
            rebuildList();
        }

        private void btnMasEdit_Click(object sender, EventArgs e)
        {
            int index = listMAS.SelectedIndex;
            if (index < 0 || listMAS.Items.Count == 0)
            {
                MessageBox.Show("Select a MAS file out of the list for editing (and if there are non, then add one)!");
                return;
            }

            MASConfig config = rfBuild.MASConfigs[index];
            new MasWindow(this, config, tbWorkingDirectory.Text);
        }

        private void btnMasDel_Click(object sender, EventArgs e)
        {
            int index = listMAS.SelectedIndex;
            if (index < 0 || listMAS.Items.Count == 0)
            {
                MessageBox.Show("Select a MAS file out of the list for deleting (and if there are non, then add one)!");
                return;
            }

            MASConfig config = rfBuild.MASConfigs[index];

            DialogResult result = MessageBox.Show("Do you want to delete " + config.MasLocation + "?", "Deleting MAS file", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                rfBuild.MASConfigs.Remove(config);
            }
            rebuildList();
        }

        private void CbSkinpackName_TextChanged(object sender, EventArgs e)
        {
            string item = CbSkinpackName.Text;

            tbName.Text = item;

            CbBaseVersion.Items.Clear();
            if (InstalledMods.ContainsKey(item))
            {
                CbBaseVersion.Items.AddRange(InstalledMods[item]);
            }

        }

        private void tsmiAbout_Click(object sender, EventArgs e)
        {
            new TheLichten.General.About(this, "https://gitlab.com/LukasLichten/MASCompiler/wikis/Version.md");
        }

        private void tsmiHelp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/LukasLichten/MASCompiler/wikis/Handbook");
        }
    }
}
