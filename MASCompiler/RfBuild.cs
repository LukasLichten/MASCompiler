﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace MASCompiler
{
    public class RfBuild
    {
        public List<MASConfig> MASConfigs { get; set; }
        public string buildPath { get; set; }
        public Dictionary<string, string> cmpinfo { get; set; }

        public string buildRFCMP(string origin, string cmppack, string rf2Loc, bool rebuild, string version, bool noVersionChange)
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Started building rfcmp");
            Console.WriteLine("");
            Console.WriteLine("Started checking files for modpackage " + cmpinfo["name"]);

            string realPath = getBuildPath(origin);
            if (!Directory.Exists(realPath))
            {
                Directory.CreateDirectory(realPath);
            }

            string hashPath = Path.Combine(realPath, "hash");
            if (!Directory.Exists(hashPath))
            {
                DirectoryInfo info = Directory.CreateDirectory(hashPath);
                info.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
            Dictionary<string, string> hashes = getHashFile(getHashFileLocation(cmpinfo["name"], origin));

            //Determines if build is neccessary
            bool needsToBeBuild = false;
            if (hashes == null)
            {
                hashes = new Dictionary<string, string>();
                needsToBeBuild = true;
            }
            else
            {
                foreach (MASConfig config in MASConfigs)
                {
                    if (!hashes.ContainsKey(config.MasLocation.ToLower()))
                    {
                        needsToBeBuild = true;
                        break;
                    }
                    string nowHash = Util.getHashCode(Path.Combine(getBuildPath(origin), config.MasLocation));

                    string thenHash = hashes[config.MasLocation.ToLower()];

                    if (nowHash != thenHash)
                    {
                        needsToBeBuild = true;
                        break;
                    }
                }
            }


            if (needsToBeBuild || rebuild)
            {
                //Setting up the cmppackinfo file
                //string cmpfile = Path.Combine(Environment.GetEnvironmentVariable("UserProfile"), "AppData\\Roaming\\cmpinfo.dat");
                //if (File.Exists(cmpfile))
                //{
                //    if (File.Exists(cmpfile + ".back"))
                //        File.Delete(cmpfile + ".back");

                //    File.Move(cmpfile, cmpfile + ".back");
                //}
                Console.WriteLine("Check completed. " + cmpinfo["name"] + " will be build!");

                string cmpfile = Path.Combine(origin, "temp.cmpinfo");
                if (File.Exists(cmpfile))
                    File.Delete(cmpfile);

                FileStream stream = new FileStream(cmpfile, FileMode.OpenOrCreate);
                StreamWriter writer = new StreamWriter(stream);

                writer.WriteLine("[Component]");
                writer.WriteLine("Name=" + cmpinfo["name"]);
                writer.WriteLine("Type=" + cmpinfo["type"]);

                //Version
                if (version == null)
                {
                    if (hashes.ContainsKey("version"))
                        version = hashes["version"];
                    double v = -1;
                    if (Double.TryParse(cmpinfo["version"], out v))
                    {
                        if (version != null)
                        {
                            if (v <= Double.Parse(version))
                            {
                                v = Double.Parse(version);

                                //Autoincrement:
                                if (!noVersionChange)
                                    v = v + 0.01;
                            }

                            version = v + "";
                            hashes["version"] = version;
                        }
                        else
                        {
                            version = v + "";
                            hashes.Add("version", version);
                        }
                    }
                    else
                    {
                        version = cmpinfo["version"];
                        hashes.Remove("version");
                    }
                }
                else
                {
                    double v = -1;
                    if (Double.TryParse(version, out v))
                    {
                        if (hashes.ContainsKey("version"))
                            hashes["version"] = v + "";
                        else
                            hashes.Add("version", v + "");
                    }
                }


                //Some static stuff
                writer.WriteLine("Version=" + version);
                writer.WriteLine("BaseVersion=" + cmpinfo["baseversion"]);
                writer.WriteLine("MinVersion=" /*+ cmpinfo["minversion"]*/);
                writer.WriteLine("Author=" + cmpinfo["author"]);
                writer.WriteLine("Date=" + DateTime.Now.ToFileTimeUtc());
                writer.WriteLine("ID=" /*+ cmpinfo["id"]*/);
                writer.WriteLine("URL=" + cmpinfo["url"]);
                writer.WriteLine("Desc=" + cmpinfo["desc"]);
                writer.WriteLine("Category=" + cmpinfo["category"]);
                writer.WriteLine("Origin=" + cmpinfo["origin"]);
                writer.WriteLine("Flags=" /*+ cmpinfo["flags"]*/ + "0");
                writer.WriteLine("CurLocation=" + "0");

                //Build Loc
                writer.WriteLine("NumLocations=" + "1");
                string rfcmpFileName = cmpinfo["name"] + "_v" + version + ".rfcmp";
                writer.WriteLine("Location=" + Path.Combine(getBuildPath(origin), rfcmpFileName));
                writer.WriteLine("NumMASFiles=" + MASConfigs.Count);
                foreach (MASConfig config in MASConfigs)
                    writer.WriteLine("MASFile=" + Path.Combine(getBuildPath(origin), config.MasLocation));

                //The rest
                writer.WriteLine("rFmFile="/* + cmpinfo["rfmfile"]*/);
                writer.WriteLine("IconFile="/* + cmpinfo["iconfile"]*/);
                writer.WriteLine("SmallIconFile="/* + cmpinfo["smalliconfile"]*/);
                writer.WriteLine("");
                writer.WriteLine("CurComponent=0");

                writer.Flush();
                writer.Close();
                stream.Close();

                Console.WriteLine("Finished preparation for packing for " + cmpinfo["name"]);
                //Packing
                //Thread thread = new Thread((a0 => Util.buildRfcmp(Util.getMASTool(rf2Loc), cmpfile)));
                Thread thread = new Thread((a0 => Util.buildRfcmpWithCmpack(cmppack, cmpfile)));

                thread.Start();

                //Setting the hashes for the mas files
                foreach (MASConfig config in MASConfigs)
                {
                    string nowHash = Util.getHashCode(Path.Combine(getBuildPath(origin), config.MasLocation));

                    if (!hashes.ContainsKey(config.MasLocation.ToLower()))
                    {
                        hashes.Add(config.MasLocation.ToLower(), nowHash);
                    }
                    else
                    {
                        hashes[config.MasLocation.ToLower()] = nowHash;
                    }
                }

                setHashFile(getHashFileLocation(cmpinfo["name"], origin), hashes);

                thread.Join();

                //if (File.Exists(cmpfile + ".back"))
                //{
                //    File.Delete(cmpfile);
                //    File.Move(cmpfile + ".back", cmpfile);
                //}

                File.Delete(cmpfile);
                Console.WriteLine("Bulding of " + rfcmpFileName + " completed");
                return version;
            }

            Console.WriteLine("Check completed. " + cmpinfo["name"] + " is up to date!");
            return null;
        }

        public void buildMASs(string origin, string rf2Loc, bool rebuild)
        {
            Console.WriteLine("Starting to build mas files...");
            Console.WriteLine("This is done in multiple threads, so the outputs of the ModMgr will look strange.");

            string realPath = getBuildPath(origin);
            if (!Directory.Exists(realPath))
            {
                Directory.CreateDirectory(realPath);
            }

            string hashPath = Path.Combine(realPath, "hash");
            if (!Directory.Exists(hashPath))
            {
                DirectoryInfo info = Directory.CreateDirectory(hashPath);
                info.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }

            List<Thread> threads = new List<Thread>();

            foreach (MASConfig config in MASConfigs)
            {
                if (config.rules.Length != 0)
                {
                    if (rebuild)
                    {
                        if (File.Exists(Path.Combine(getBuildPath(origin), config.MasLocation)))
                            File.Delete(Path.Combine(getBuildPath(origin), config.MasLocation));

                        if (File.Exists(getHashFileLocation(config, origin)))
                            File.Delete(getHashFileLocation(config, origin));
                    }

                    Thread t = new Thread((a0 => buildMasFile(config, origin, rf2Loc)));
                    t.Start();
                    threads.Add(t);

                    //Stopping the error through building
                    t.Join();
                }
                else
                {
                    string masloc = Path.Combine(getBuildPath(origin), config.MasLocation);
                    if (!File.Exists(masloc))
                    {
                        //This avoids potential issues with missing mas files
                        string[] fillerFile = { Path.Combine(getBuildPath(origin), "Filler.dat") };

                        File.Create(fillerFile[0]);

                        Util.addFilesToMas(Util.getModMgr(rf2Loc), masloc, fillerFile).WaitForExit();
                        Console.WriteLine("Warning: " + config.MasLocation + " should be a static mas, but does not exist, so a filler was created!");
                    }

                }
            }

            //Waiting for the threads to finish:
            foreach (Thread t in threads)
                t.Join();

            Console.WriteLine("Finished MAS building!");
        }

        private void buildMasFile(MASConfig config, string origin, string rf2Loc)
        {
            Console.WriteLine("Started checking files for " + config.MasLocation);
            string[] files = config.getFileLocations(origin);
            List<string> toupdate = new List<string>();

            Dictionary<string, string> hashes = getHashFile(getHashFileLocation(config, origin));

            string masHash = Util.getHashCode(Path.Combine(getBuildPath(origin), config.MasLocation));

            if (hashes == null || !hashes.ContainsKey("masHash") || masHash != hashes["masHash"])
            {
                toupdate.AddRange(files);
                hashes = null;
            }
            else
            {
                for (int i = 0; i < files.Length; i++)
                {
                    string f = files[i];

                    string nowHash = Util.getHashCode(f);

                    string realtivPath = f.Replace(origin, "");
                    if (hashes.ContainsKey(realtivPath))
                    {

                        string thenHash = hashes[realtivPath];

                        if (nowHash != thenHash)
                        {
                            toupdate.Add(f);
                            hashes[realtivPath] = nowHash;
                        }

                    }
                    else
                    {
                        toupdate.Add(f);
                        hashes.Add(realtivPath, nowHash);
                    }
                }
            }

            Process pro = null;

            Console.WriteLine("Finished checking files for " + config.MasLocation + ". " + toupdate.Count + " need to be updated.");

            if (toupdate.Count != 0)
            {
                pro = Util.addFilesToMas(Util.getModMgr(rf2Loc), Path.Combine(getBuildPath(origin), config.MasLocation), toupdate.ToArray());
                Console.WriteLine("Started packaging for " + config.MasLocation + "...");
            }


            if (hashes == null)
            {
                hashes = new Dictionary<string, string>();
                //Hashes needs to be build
                foreach (string f in files)
                {
                    string nowHash = Util.getHashCode(f);

                    string realtivPath = f.Replace(origin, "");
                    hashes.Add(realtivPath, nowHash);
                }
            }

            

            if (pro != null)
            {
                pro.WaitForExit();

                //Moving the mas file out of the temporary folder
                string masPath = Path.Combine(getBuildPath(origin), config.MasLocation);

                string specialFolder = masPath.Replace(Path.GetExtension(masPath), "");
                File.Copy(Path.Combine(specialFolder, "temp", Path.GetFileName(masPath)), masPath, true);

                //Cleaning up the mess
                Directory.Delete(specialFolder, true);

                Console.WriteLine("Finished packaging for " + config.MasLocation + "!");
            }
            else
            {
                Console.WriteLine("Finished building " + config.MasLocation + "!");
            }

            if (!hashes.ContainsKey("masHash"))
                hashes.Add("masHash", "");
            
            hashes["masHash"] = Util.getHashCode(Path.Combine(getBuildPath(origin), config.MasLocation));
            setHashFile(getHashFileLocation(config, origin), hashes);
        }

        private string getBuildPath(string origin)
        {
            string buildPath = this.buildPath;
            if (buildPath.StartsWith("/"))
                buildPath = buildPath.Substring(1, buildPath.Length - 1);
            if (buildPath.EndsWith("/"))
                buildPath = buildPath.Substring(0, buildPath.Length - 1);


            return Path.Combine(origin, buildPath);
        }

        private string getHashFileLocation(MASConfig config, string origin)
        {
            return getHashFileLocation(config.MasLocation, origin);
        }

        private string getHashFileLocation(string filename, string origin)
        {
            string path = getBuildPath(origin);
            if (!Directory.Exists(path))
                return null;

            path = Path.Combine(path, "hash");
            if (!Directory.Exists(path))
                return null;

            path = Path.Combine(path, filename + ".hash");
            return path;
        }

        private Dictionary<string, string> getHashFile(string path)
        {
            if (path == null || !File.Exists(path))
                return null;

            BinaryFormatter formater = new BinaryFormatter();

            FileStream stream = new FileStream(path, FileMode.Open);
            object obj = formater.Deserialize(stream);

            stream.Close();

            if (obj.GetType().Equals(typeof(Dictionary<string, string>)))
                return (Dictionary<string, string>)obj;

            return null;
        }

        private void setHashFile(string path, Dictionary<string, string> hashData)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            BinaryFormatter formater = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.OpenOrCreate);

            formater.Serialize(stream, hashData);

            stream.Flush();
            stream.Close();
        }
    }
}
