﻿namespace MASCompiler
{
    partial class MasWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listRules = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listFiles = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddRule = new System.Windows.Forms.Button();
            this.btnDelRule = new System.Windows.Forms.Button();
            this.btnEditRule = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnNegate = new System.Windows.Forms.Button();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.btnSelectFolderRec = new System.Windows.Forms.Button();
            this.btnFileTypeFilter = new System.Windows.Forms.Button();
            this.btnRuleUp = new System.Windows.Forms.Button();
            this.btnRuleDown = new System.Windows.Forms.Button();
            this.gbEditRule = new System.Windows.Forms.GroupBox();
            this.tbRule = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.gbEditRule.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(50, 10);
            this.tbName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(158, 20);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Name:";
            // 
            // listRules
            // 
            this.listRules.FormattingEnabled = true;
            this.listRules.Location = new System.Drawing.Point(11, 50);
            this.listRules.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listRules.Name = "listRules";
            this.listRules.Size = new System.Drawing.Size(164, 212);
            this.listRules.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Rules:";
            // 
            // listFiles
            // 
            this.listFiles.FormattingEnabled = true;
            this.listFiles.Location = new System.Drawing.Point(263, 50);
            this.listFiles.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listFiles.Name = "listFiles";
            this.listFiles.Size = new System.Drawing.Size(223, 212);
            this.listFiles.TabIndex = 13;
            this.toolTip1.SetToolTip(this.listFiles, "Files included through the rules. To remove, or add files, you need to add/remove" +
        " rules");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Files included:";
            this.toolTip1.SetToolTip(this.label2, "Files included through the rules. To remove, or add files, you need to add/remove" +
        " rules");
            // 
            // btnAddRule
            // 
            this.btnAddRule.Location = new System.Drawing.Point(179, 50);
            this.btnAddRule.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddRule.Name = "btnAddRule";
            this.btnAddRule.Size = new System.Drawing.Size(80, 22);
            this.btnAddRule.TabIndex = 15;
            this.btnAddRule.Text = "Add";
            this.btnAddRule.UseVisualStyleBackColor = true;
            this.btnAddRule.Click += new System.EventHandler(this.btnAddRule_Click);
            // 
            // btnDelRule
            // 
            this.btnDelRule.Location = new System.Drawing.Point(179, 104);
            this.btnDelRule.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDelRule.Name = "btnDelRule";
            this.btnDelRule.Size = new System.Drawing.Size(80, 22);
            this.btnDelRule.TabIndex = 16;
            this.btnDelRule.Text = "Remove";
            this.btnDelRule.UseVisualStyleBackColor = true;
            this.btnDelRule.Click += new System.EventHandler(this.btnDelRule_Click);
            // 
            // btnEditRule
            // 
            this.btnEditRule.Location = new System.Drawing.Point(179, 77);
            this.btnEditRule.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEditRule.Name = "btnEditRule";
            this.btnEditRule.Size = new System.Drawing.Size(80, 22);
            this.btnEditRule.TabIndex = 17;
            this.btnEditRule.Text = "Edit";
            this.btnEditRule.UseVisualStyleBackColor = true;
            this.btnEditRule.Click += new System.EventHandler(this.btnEditRule_Click);
            // 
            // btnNegate
            // 
            this.btnNegate.Location = new System.Drawing.Point(413, 14);
            this.btnNegate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNegate.Name = "btnNegate";
            this.btnNegate.Size = new System.Drawing.Size(56, 24);
            this.btnNegate.TabIndex = 1;
            this.btnNegate.Text = "Negate";
            this.toolTip1.SetToolTip(this.btnNegate, "This makes the rule filter instead of adding the files that apply to this rule, b" +
        "y adding a \'!\' infront");
            this.btnNegate.UseVisualStyleBackColor = true;
            this.btnNegate.Click += new System.EventHandler(this.btnNegate_Click);
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(148, 43);
            this.btnSelectFolder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(85, 24);
            this.btnSelectFolder.TabIndex = 2;
            this.btnSelectFolder.Text = "Select Folder";
            this.toolTip1.SetToolTip(this.btnSelectFolder, "Creates a rule that adds files from a specific folder (but NO sub folders)");
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Location = new System.Drawing.Point(237, 43);
            this.btnSelectFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(85, 24);
            this.btnSelectFile.TabIndex = 3;
            this.btnSelectFile.Text = "Select File";
            this.toolTip1.SetToolTip(this.btnSelectFile, "Creates a rule that adds this specific file");
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // btnSelectFolderRec
            // 
            this.btnSelectFolderRec.Location = new System.Drawing.Point(4, 43);
            this.btnSelectFolderRec.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSelectFolderRec.Name = "btnSelectFolderRec";
            this.btnSelectFolderRec.Size = new System.Drawing.Size(139, 24);
            this.btnSelectFolderRec.TabIndex = 4;
            this.btnSelectFolderRec.Text = "Select Folder Recursively ";
            this.toolTip1.SetToolTip(this.btnSelectFolderRec, "Creates a rule that adds files from this folder (and ALL subfolders)");
            this.btnSelectFolderRec.UseVisualStyleBackColor = true;
            this.btnSelectFolderRec.Click += new System.EventHandler(this.btnSelectFolderRec_Click);
            // 
            // btnFileTypeFilter
            // 
            this.btnFileTypeFilter.Location = new System.Drawing.Point(326, 43);
            this.btnFileTypeFilter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFileTypeFilter.Name = "btnFileTypeFilter";
            this.btnFileTypeFilter.Size = new System.Drawing.Size(85, 24);
            this.btnFileTypeFilter.TabIndex = 5;
            this.btnFileTypeFilter.Text = "File Type Filter";
            this.toolTip1.SetToolTip(this.btnFileTypeFilter, "Creates a rule that filters out files with a certain ending");
            this.btnFileTypeFilter.UseVisualStyleBackColor = true;
            this.btnFileTypeFilter.Click += new System.EventHandler(this.btnFileTypeFilter_Click);
            // 
            // btnRuleUp
            // 
            this.btnRuleUp.Location = new System.Drawing.Point(179, 131);
            this.btnRuleUp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRuleUp.Name = "btnRuleUp";
            this.btnRuleUp.Size = new System.Drawing.Size(80, 22);
            this.btnRuleUp.TabIndex = 18;
            this.btnRuleUp.Text = "Move Up";
            this.btnRuleUp.UseVisualStyleBackColor = true;
            this.btnRuleUp.Click += new System.EventHandler(this.btnRuleUp_Click);
            // 
            // btnRuleDown
            // 
            this.btnRuleDown.Location = new System.Drawing.Point(179, 158);
            this.btnRuleDown.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRuleDown.Name = "btnRuleDown";
            this.btnRuleDown.Size = new System.Drawing.Size(80, 22);
            this.btnRuleDown.TabIndex = 19;
            this.btnRuleDown.Text = "Move Down";
            this.btnRuleDown.UseVisualStyleBackColor = true;
            this.btnRuleDown.Click += new System.EventHandler(this.btnRuleDown_Click);
            // 
            // gbEditRule
            // 
            this.gbEditRule.Controls.Add(this.btnFileTypeFilter);
            this.gbEditRule.Controls.Add(this.btnSelectFolderRec);
            this.gbEditRule.Controls.Add(this.btnSelectFile);
            this.gbEditRule.Controls.Add(this.btnSelectFolder);
            this.gbEditRule.Controls.Add(this.btnNegate);
            this.gbEditRule.Controls.Add(this.tbRule);
            this.gbEditRule.Location = new System.Drawing.Point(11, 266);
            this.gbEditRule.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEditRule.Name = "gbEditRule";
            this.gbEditRule.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEditRule.Size = new System.Drawing.Size(474, 78);
            this.gbEditRule.TabIndex = 20;
            this.gbEditRule.TabStop = false;
            this.gbEditRule.Text = "Edit Rule";
            // 
            // tbRule
            // 
            this.tbRule.Location = new System.Drawing.Point(4, 17);
            this.tbRule.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbRule.Name = "tbRule";
            this.tbRule.Size = new System.Drawing.Size(405, 20);
            this.tbRule.TabIndex = 0;
            this.tbRule.TextChanged += new System.EventHandler(this.tbRule_TextChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(179, 184);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(80, 22);
            this.btnRefresh.TabIndex = 21;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // MasWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 354);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.gbEditRule);
            this.Controls.Add(this.btnRuleDown);
            this.Controls.Add(this.btnRuleUp);
            this.Controls.Add(this.btnEditRule);
            this.Controls.Add(this.btnDelRule);
            this.Controls.Add(this.btnAddRule);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listFiles);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listRules);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MasWindow";
            this.Text = "MasWindow";
            this.gbEditRule.ResumeLayout(false);
            this.gbEditRule.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listRules;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listFiles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddRule;
        private System.Windows.Forms.Button btnDelRule;
        private System.Windows.Forms.Button btnEditRule;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnRuleUp;
        private System.Windows.Forms.Button btnRuleDown;
        private System.Windows.Forms.GroupBox gbEditRule;
        private System.Windows.Forms.TextBox tbRule;
        private System.Windows.Forms.Button btnNegate;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Button btnSelectFolderRec;
        private System.Windows.Forms.Button btnFileTypeFilter;
        private System.Windows.Forms.Button btnRefresh;
    }
}