﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MASCompiler
{
    public partial class MasWindow : Form
    {
        private MainWindow master;
        private MASConfig config;
        private string origin;

        private int editing = -1;

        public MasWindow(MainWindow master, MASConfig config, string origin)
        {
            InitializeComponent();
            Owner = master;
            this.master = master;
            this.config = config;
            this.origin = origin;

            this.FormClosing += new FormClosingEventHandler(saving);
            this.Show();

            //Fill in
            gbEditRule.Visible = false;
            tbName.Text = config.MasLocation;
            rebuildRules();
        }

        private void rebuildRules()
        {
            listRules.Items.Clear();

            foreach (string rule in config.rules)
                listRules.Items.Add(rule);

            rebuildFileList();
        }

        private void rebuildFileList()
        {
            listFiles.Items.Clear();

            string[] files = config.getFileLocations(origin);

            foreach (string f in files)
            {
                string relativ = f.Replace(origin, "");
                listFiles.Items.Add(relativ);
            }
        }

        private void saving(object sender, FormClosingEventArgs e)
        {
            if (!tbName.Text.ToLower().Contains(".mas"))
                tbName.Text = tbName.Text + ".mas";

            //Build config
            config.MasLocation = tbName.Text;

            if (master.rfBuild.MASConfigs == null)
                master.rfBuild.MASConfigs = new List<MASConfig>();

            if (!master.rfBuild.MASConfigs.Contains(config))
            {
                master.rfBuild.MASConfigs.Add(config);
            }

            master.rebuildList();
        }

        private string getRule()
        {
            int index = listRules.SelectedIndex;
            if (index < 0 || listRules.Items.Count == 0)
            {
                return null;
            }

            return config.rules[index];
        }

        private void btnAddRule_Click(object sender, EventArgs e)
        {
            gbEditRule.Visible = false;
            string[] rules = new string[config.rules.Length + 1];

            for (int i = 0; i < config.rules.Length; i++)
            {
                rules[i] = config.rules[i];
            }

            

            editing = rules.Length - 1;
            rules[editing] = "";

            config.rules = rules;

            tbRule.Text = rules[editing];
            rebuildRules();
            
            gbEditRule.Visible = true;
            listRules.SelectedIndex = editing;
        }
        
        private void btnEditRule_Click(object sender, EventArgs e)
        {
            string rule = getRule();
            if (rule == null)
            {
                MessageBox.Show("You must select a rule to edit it");
                return;
            }


            editing = listRules.SelectedIndex;
            tbRule.Text = config.rules[editing];
            rebuildRules();
            gbEditRule.Visible = true;

            listRules.SelectedIndex = editing;
        }

        private void btnDelRule_Click(object sender, EventArgs e)
        {
            string rule = getRule();
            if (rule == null)
            {
                MessageBox.Show("You must select a rule to remove it");
                return;
            }

            DialogResult result = MessageBox.Show("Do you want to delete rule " + rule + " ?", "Deleting a Rule", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                List<string> rules = new List<string>();
                for (int i = 0; i < config.rules.Length; i++)
                {
                    if (i != listRules.SelectedIndex)
                        rules.Add(config.rules[i]);
                }

                

                if (editing > listRules.SelectedIndex)
                    editing--;
                else if (editing == listRules.SelectedIndex)
                {
                    editing = -1;
                    tbRule.Text = "";
                }

                config.rules = rules.ToArray();
                rebuildRules();
            }
        }

        private void btnRuleUp_Click(object sender, EventArgs e)
        {
            string rule = getRule();
            if (rule == null)
            {
                MessageBox.Show("You must select a rule to move it up");
                return;
            }
            else if (listRules.SelectedIndex == 0)
            {
                MessageBox.Show("You can not move the first rule up");
                return;
            }

            int index = listRules.SelectedIndex;
            string upper = config.rules[index - 1];
            config.rules[index - 1] = rule;
            config.rules[index] = upper;

            if (editing == index)
                editing--;
            else if (editing == index - 1)
                editing++;

            
            rebuildRules();
            listRules.SelectedIndex = index-1;
        }

        private void btnRuleDown_Click(object sender, EventArgs e)
        {
            string rule = getRule();
            if (rule == null)
            {
                MessageBox.Show("You must select a rule to move it down");
                return;
            }
            else if (listRules.SelectedIndex == (config.rules.Length - 1))
            {
                MessageBox.Show("You can not move the last rule down");
                return;
            }
            int index = listRules.SelectedIndex;
            string downer = config.rules[index + 1];
            config.rules[index + 1] = rule;
            config.rules[index] = downer;

            if (editing == index)
                editing++;
            else if (editing == index + 1)
                editing--;

            rebuildRules();
            listRules.SelectedIndex = index+1;
        }


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            rebuildFileList();
        }

        //Rule Creator
        private void updateRule()
        {
            int index = listRules.SelectedIndex;
            if (editing == -1)
                return;

            config.rules[editing] = tbRule.Text;
            rebuildRules();
            if (index >= 0 && index < config.rules.Length)
                listRules.SelectedIndex = index;
        }


        private void btnNegate_Click(object sender, EventArgs e)
        {
            tbRule.Text = tbRule.Text.Trim();

            if (tbRule.Text.StartsWith("!"))
            {
                if (tbRule.Text.Contains("*."))
                {
                    MessageBox.Show("You can not remove the negation of a file type filter!");
                    return;
                }
                tbRule.Text = tbRule.Text.Substring(1, tbRule.Text.Length - 1).Trim();
            }
            else
            {
                tbRule.Text = "!" + tbRule.Text;
            }

            updateRule();
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;
            if (origin != "" && origin != null)
                dialog.SelectedPath = origin;

            if (DialogResult.OK == dialog.ShowDialog())
            {
                string path = dialog.SelectedPath;
                if (!path.Contains(origin))
                {
                    MessageBox.Show("You can not select folders outside the working directory.\n Please check your Working directory in the Main Window.");
                    tbRule.Text = "";
                    return;
                }
                path = path.Replace(origin, "");

                path = path.Replace(Path.DirectorySeparatorChar, '/');
                path = path + "/";

                tbRule.Text = path;

                updateRule();
            }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open a Rf-build file";
            dialog.Multiselect = false;
            dialog.InitialDirectory = origin;

            DialogResult result = dialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            string path = dialog.FileName;
            if (!path.Contains(origin))
            {
                MessageBox.Show("You can not select a file outside the working directory.\n Please check your Working directory in the Main Window.");
                tbRule.Text = "";
                return;
            }
            path = path.Replace(origin, "");

            path = path.Replace(Path.DirectorySeparatorChar, '/');
            tbRule.Text = path;

            updateRule();
        }

        private void btnSelectFolderRec_Click(object sender, EventArgs e)
        {
            btnSelectFolder_Click(sender, e);
            if (tbRule.Text == "")
                return;

            tbRule.Text = tbRule.Text + "*";
            updateRule();
        }

        private void btnFileTypeFilter_Click(object sender, EventArgs e)
        {
            tbRule.Text = "!*.YourFileEnding";
            updateRule();
        }

        private void tbRule_TextChanged(object sender, EventArgs e)
        {
            updateRule();
        }
    }
}
