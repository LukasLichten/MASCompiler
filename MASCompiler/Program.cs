﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MASCompiler
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int pid);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool FreeConsole();


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args == null || args.Length == 0)
            {
                
                Application.Run(new MainWindow());
            }
            else
                commandLineMode(args);


        }

        private static void commandLineMode(string[] args)
        {
            // get console output
            if (!AttachConsole(-1))
                AllocConsole();

            if (args[0] == null || args[0].StartsWith("-"))
            {
                Console.WriteLine("The format is: MASCompiler.exe [rfbuild file] <options...>");
                Console.WriteLine("Options avaible:");
                Console.WriteLine("-r           \tRecompiling everything. Takes longer");
                Console.WriteLine("-mas         \tOnly updates the mas files, but not the rfcmp");
                Console.WriteLine("-cmp         \tOnly updates the rfcmp out of the preexisting mas files");
                Console.WriteLine("-v=[number]  \tCompiles the specific version number");
                Console.WriteLine("-vno         \tCompiles without an auto increase of the version number (usally 1.53 to 1.54, if you build 1.53 prior. NOT needed when using -v=[number])");
                Console.WriteLine("-open        \tOpens the file in the graphical client");
                Console.WriteLine("-rf2=[rf2Loc]\tSets the default Rfactor 2 path (Usually this should be found automatically)");
                return;
            }
            
            //Some commandline stuff
            string buildFile = args[0];
            string origin = Path.GetDirectoryName(Application.ExecutablePath);
            if (!buildFile.Contains(":"))
                buildFile = Path.Combine(origin, buildFile);
            else
                origin = Path.GetDirectoryName(buildFile);

            if (!File.Exists(buildFile) || Path.GetExtension(buildFile).ToLower() != ".rfbuild")
            {
                Console.WriteLine("You have entered a non valid file path.");
                Console.WriteLine("Make sure the file exists and has the rfbuild extension");
                return;
            }

            ConfigReader reader;
            RfBuild[] rfbuilds;

            if (Util.containsArg(args, "-open") != -1)
            {
                FreeConsole();
                reader = new ConfigReader(buildFile);
                rfbuilds = reader.read();
                MainWindow main = new MainWindow();
                main.openRfBuild(rfbuilds[0], buildFile);
                Application.Run(main);
                return;
            }

            string cmppack = Path.Combine(origin, "cmppack.exe");
            Thread thread = new Thread((a0 => Util.extractCmppack(cmppack)));
            thread.Start();

            reader = new ConfigReader(buildFile);
            rfbuilds = reader.read();

            string rf2Loc = null;
            if (Util.containsArg(args, "-rf2=") != -1)
            {
                rf2Loc = Util.getValuePair(args[Util.containsArg(args, "-rf2=")]).Value;
                if (!Util.checkForSoftware(rf2Loc))
                    rf2Loc = null;
            }

            if (rf2Loc == null)
                rf2Loc = Util.getRFactor2Loc(origin);


            if (rf2Loc != null)
                Util.setMAS2PathForCmppack(rf2Loc);
            

            thread.Join();

            if (rf2Loc == null)
            {
                Util.deleteCmppack(cmppack);
                Console.WriteLine("rf2 folder not found! Please use -rf2=[rf2Loc] to set the rf2 Folder manually!");
                return;
            }

            //Reading additional parameters
            int onlyOneMode = 0;
            if (Util.containsArg(args, "-mas") != -1)
                onlyOneMode = 1;
            else if (Util.containsArg(args, "-cmp") != -1)
                onlyOneMode = 2;

            bool rebuild = Util.containsArg(args, "-r") != -1;

            string version = null;
            if (Util.containsArg(args, "-v=") != -1)
                version = Util.getValuePair(args[Util.containsArg(args, "-v=")]).Value;

            bool noVersionChange = Util.containsArg(args, "-vno") != -1;

            // Were the work is done
            try
            {
                foreach (RfBuild builder in rfbuilds)
                {
                    if (onlyOneMode != 2)
                        builder.buildMASs(origin, rf2Loc, rebuild);
                    string v = null;
                    if (onlyOneMode != 1)
                        v = builder.buildRFCMP(origin, cmppack, rf2Loc, rebuild, version, noVersionChange);
                        

                    if (v != null)
                        version = v;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("And Exception has occured!");
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Util.deleteCmppack(cmppack);
            }
            

            
        }
    }
}
