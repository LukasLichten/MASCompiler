﻿namespace MASCompiler
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.tbWorkingDirectory = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRun = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.btnWorkingDirBrowse = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbBuildPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbDesc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbURL = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbAuthor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbOrigin = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnMasDel = new System.Windows.Forms.Button();
            this.btnMasEdit = new System.Windows.Forms.Button();
            this.btnMasAdd = new System.Windows.Forms.Button();
            this.listMAS = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.GbSkinpack = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TsmiIsSkinpack = new System.Windows.Forms.ToolStripMenuItem();
            this.CbSkinpackName = new System.Windows.Forms.ComboBox();
            this.CbBaseVersion = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GbSkinpack.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Working Directory:";
            // 
            // tbWorkingDirectory
            // 
            this.tbWorkingDirectory.Location = new System.Drawing.Point(107, 29);
            this.tbWorkingDirectory.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbWorkingDirectory.Name = "tbWorkingDirectory";
            this.tbWorkingDirectory.Size = new System.Drawing.Size(350, 20);
            this.tbWorkingDirectory.TabIndex = 1;
            this.toolTip1.SetToolTip(this.tbWorkingDirectory, "Directory containing the rfbuild file and is root folder for all MAS Rules");
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmiRun,
            this.toolStripMenuItem2,
            this.TsmiIsSkinpack});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(689, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOpen,
            this.tsmiSave});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 24);
            this.toolStripMenuItem1.Text = "File";
            // 
            // tsmiOpen
            // 
            this.tsmiOpen.Name = "tsmiOpen";
            this.tsmiOpen.Size = new System.Drawing.Size(180, 22);
            this.tsmiOpen.Text = "Open";
            this.tsmiOpen.Click += new System.EventHandler(this.tsmiOpen_Click);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.Size = new System.Drawing.Size(180, 22);
            this.tsmiSave.Text = "Save";
            this.tsmiSave.Click += new System.EventHandler(this.tsmiSave_Click);
            // 
            // tsmiRun
            // 
            this.tsmiRun.Image = global::MASCompiler.Properties.Resources.Arrow;
            this.tsmiRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsmiRun.Name = "tsmiRun";
            this.tsmiRun.Size = new System.Drawing.Size(60, 24);
            this.tsmiRun.Text = "Run";
            this.tsmiRun.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsmiRun.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.tsmiRun.Click += new System.EventHandler(this.tsmiRun_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAbout,
            this.tsmiHelp});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(44, 24);
            this.toolStripMenuItem2.Text = "Help";
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(180, 22);
            this.tsmiAbout.Text = "About";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // tsmiHelp
            // 
            this.tsmiHelp.Name = "tsmiHelp";
            this.tsmiHelp.Size = new System.Drawing.Size(180, 22);
            this.tsmiHelp.Text = "Help!";
            this.tsmiHelp.Click += new System.EventHandler(this.tsmiHelp_Click);
            // 
            // btnWorkingDirBrowse
            // 
            this.btnWorkingDirBrowse.Location = new System.Drawing.Point(460, 27);
            this.btnWorkingDirBrowse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWorkingDirBrowse.Name = "btnWorkingDirBrowse";
            this.btnWorkingDirBrowse.Size = new System.Drawing.Size(56, 23);
            this.btnWorkingDirBrowse.TabIndex = 3;
            this.btnWorkingDirBrowse.Text = "Browse";
            this.btnWorkingDirBrowse.UseVisualStyleBackColor = true;
            this.btnWorkingDirBrowse.Click += new System.EventHandler(this.btnWorkingDirBrowse_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(582, 29);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(99, 20);
            this.tbFileName.TabIndex = 5;
            this.toolTip1.SetToolTip(this.tbFileName, "Filename (without .rfbuild ending)");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(521, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "File Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbBuildPath);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbDesc);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbURL);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbAuthor);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbCategory);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbOrigin);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbType);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbVersion);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(11, 54);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(351, 306);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "rfcmp";
            // 
            // tbBuildPath
            // 
            this.tbBuildPath.Location = new System.Drawing.Point(98, 277);
            this.tbBuildPath.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBuildPath.Name = "tbBuildPath";
            this.tbBuildPath.Size = new System.Drawing.Size(128, 20);
            this.tbBuildPath.TabIndex = 24;
            this.toolTip1.SetToolTip(this.tbBuildPath, "The folder (insde of the working directory) where the rfcmp and mas files will be" +
        " put after building. This folder is created when needed");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 280);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Realtiv Buildpath:";
            // 
            // tbDesc
            // 
            this.tbDesc.AcceptsReturn = true;
            this.tbDesc.Location = new System.Drawing.Point(7, 140);
            this.tbDesc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbDesc.Multiline = true;
            this.tbDesc.Name = "tbDesc";
            this.tbDesc.Size = new System.Drawing.Size(341, 133);
            this.tbDesc.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 124);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Description:";
            // 
            // tbURL
            // 
            this.tbURL.Location = new System.Drawing.Point(46, 87);
            this.tbURL.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbURL.Name = "tbURL";
            this.tbURL.Size = new System.Drawing.Size(302, 20);
            this.tbURL.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 89);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "URL:";
            // 
            // tbAuthor
            // 
            this.tbAuthor.Location = new System.Drawing.Point(46, 64);
            this.tbAuthor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbAuthor.Name = "tbAuthor";
            this.tbAuthor.Size = new System.Drawing.Size(158, 20);
            this.tbAuthor.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Author:";
            // 
            // cbCategory
            // 
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(256, 41);
            this.cbCategory.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(91, 21);
            this.cbCategory.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(200, 43);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Category:";
            // 
            // cbOrigin
            // 
            this.cbOrigin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrigin.FormattingEnabled = true;
            this.cbOrigin.Location = new System.Drawing.Point(256, 64);
            this.cbOrigin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbOrigin.Name = "cbOrigin";
            this.cbOrigin.Size = new System.Drawing.Size(91, 21);
            this.cbOrigin.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(214, 67);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Origin:";
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(46, 40);
            this.cbType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(92, 21);
            this.cbType.TabIndex = 12;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 43);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Type:";
            // 
            // tbVersion
            // 
            this.tbVersion.Location = new System.Drawing.Point(256, 17);
            this.tbVersion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.Size = new System.Drawing.Size(91, 20);
            this.tbVersion.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Version:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(46, 17);
            this.tbName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(158, 20);
            this.tbName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnMasDel);
            this.groupBox2.Controls.Add(this.btnMasEdit);
            this.groupBox2.Controls.Add(this.btnMasAdd);
            this.groupBox2.Controls.Add(this.listMAS);
            this.groupBox2.Location = new System.Drawing.Point(367, 55);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(314, 305);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MAS";
            // 
            // btnMasDel
            // 
            this.btnMasDel.Location = new System.Drawing.Point(237, 74);
            this.btnMasDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnMasDel.Name = "btnMasDel";
            this.btnMasDel.Size = new System.Drawing.Size(72, 23);
            this.btnMasDel.TabIndex = 3;
            this.btnMasDel.Text = "Delete";
            this.btnMasDel.UseVisualStyleBackColor = true;
            this.btnMasDel.Click += new System.EventHandler(this.btnMasDel_Click);
            // 
            // btnMasEdit
            // 
            this.btnMasEdit.Location = new System.Drawing.Point(237, 46);
            this.btnMasEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnMasEdit.Name = "btnMasEdit";
            this.btnMasEdit.Size = new System.Drawing.Size(72, 23);
            this.btnMasEdit.TabIndex = 2;
            this.btnMasEdit.Text = "Edit";
            this.btnMasEdit.UseVisualStyleBackColor = true;
            this.btnMasEdit.Click += new System.EventHandler(this.btnMasEdit_Click);
            // 
            // btnMasAdd
            // 
            this.btnMasAdd.Location = new System.Drawing.Point(237, 19);
            this.btnMasAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnMasAdd.Name = "btnMasAdd";
            this.btnMasAdd.Size = new System.Drawing.Size(72, 23);
            this.btnMasAdd.TabIndex = 1;
            this.btnMasAdd.Text = "Add";
            this.btnMasAdd.UseVisualStyleBackColor = true;
            this.btnMasAdd.Click += new System.EventHandler(this.btnMasAdd_Click);
            // 
            // listMAS
            // 
            this.listMAS.FormattingEnabled = true;
            this.listMAS.Location = new System.Drawing.Point(14, 17);
            this.listMAS.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listMAS.Name = "listMAS";
            this.listMAS.Size = new System.Drawing.Size(219, 251);
            this.listMAS.TabIndex = 0;
            this.toolTip1.SetToolTip(this.listMAS, "List of MAS files that will be build and included in the rfcmp");
            // 
            // GbSkinpack
            // 
            this.GbSkinpack.Controls.Add(this.CbBaseVersion);
            this.GbSkinpack.Controls.Add(this.CbSkinpackName);
            this.GbSkinpack.Controls.Add(this.label12);
            this.GbSkinpack.Controls.Add(this.label13);
            this.GbSkinpack.Location = new System.Drawing.Point(11, 365);
            this.GbSkinpack.Name = "GbSkinpack";
            this.GbSkinpack.Size = new System.Drawing.Size(670, 44);
            this.GbSkinpack.TabIndex = 8;
            this.GbSkinpack.TabStop = false;
            this.GbSkinpack.Text = "Skinpack";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(209, 21);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Base Version:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 21);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Name:";
            // 
            // TsmiIsSkinpack
            // 
            this.TsmiIsSkinpack.BackColor = System.Drawing.Color.Transparent;
            this.TsmiIsSkinpack.CheckOnClick = true;
            this.TsmiIsSkinpack.Name = "TsmiIsSkinpack";
            this.TsmiIsSkinpack.Size = new System.Drawing.Size(88, 24);
            this.TsmiIsSkinpack.Text = "Is A Skinpack";
            this.TsmiIsSkinpack.ToolTipText = "This enables settings to allow you to build this as a upgrade to an existing item" +
    "";
            // 
            // CbSkinpackName
            // 
            this.CbSkinpackName.FormattingEnabled = true;
            this.CbSkinpackName.Location = new System.Drawing.Point(46, 18);
            this.CbSkinpackName.Name = "CbSkinpackName";
            this.CbSkinpackName.Size = new System.Drawing.Size(158, 21);
            this.CbSkinpackName.TabIndex = 15;
            // 
            // CbBaseVersion
            // 
            this.CbBaseVersion.FormattingEnabled = true;
            this.CbBaseVersion.Location = new System.Drawing.Point(286, 18);
            this.CbBaseVersion.Name = "CbBaseVersion";
            this.CbBaseVersion.Size = new System.Drawing.Size(107, 21);
            this.CbBaseVersion.TabIndex = 16;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 421);
            this.Controls.Add(this.GbSkinpack);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnWorkingDirBrowse);
            this.Controls.Add(this.tbWorkingDirectory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainWindow";
            this.Text = "MAS Compiler";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.GbSkinpack.ResumeLayout(false);
            this.GbSkinpack.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbWorkingDirectory;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
        private System.Windows.Forms.Button btnWorkingDirBrowse;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbOrigin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAuthor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbURL;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbBuildPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbDesc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnMasDel;
        private System.Windows.Forms.Button btnMasEdit;
        private System.Windows.Forms.Button btnMasAdd;
        private System.Windows.Forms.ListBox listMAS;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiRun;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
        private System.Windows.Forms.GroupBox GbSkinpack;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem TsmiIsSkinpack;
        private System.Windows.Forms.ComboBox CbSkinpackName;
        private System.Windows.Forms.ComboBox CbBaseVersion;
    }
}