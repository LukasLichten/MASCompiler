﻿using MASCompiler.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Automation;
using System.Windows.Forms;

namespace MASCompiler
{
    public static class Util
    {
        public static bool checkForInvalidChars(string path)
        {
            char[] invalid = Path.GetInvalidPathChars();
            foreach (char item in path)
            {
                foreach (char inv in invalid)
                {
                    if (item == inv)
                        return false;
                }
            }

            return true;
        }

        public static Dictionary<string, string> ListToDict(List<string> list, int startIndex, int endIndex)
        {
            if (startIndex >= endIndex || endIndex > list.Count || startIndex < 0 || list == null)
            {
                throw new ArgumentException("Illegal Arguments used while creating a Dictionary");
            }

            Dictionary<string, string> dict = new Dictionary<string, string>(endIndex - startIndex);

            for (int i = startIndex; i < endIndex; i++)
            {
                try
                {
                    KeyValuePair<string, string> pair = getValuePair(list[i]);

                    if (pair.Key == null)
                        throw new NullReferenceException();

                    if (dict.ContainsKey(pair.Key))
                    {
                        dict[pair.Key] = pair.Value;
                    }
                    else
                    {
                        dict.Add(pair.Key, pair.Value);
                    }
                }
                catch
                {
                    //just continue
                }

            }

            return dict;
        }

        public static KeyValuePair<string, string> getValuePair(string line)
        {
            KeyValuePair<string, string> pair = new KeyValuePair<string, string>(null, null);
            if (line == null)
                return pair;


            line = line.Replace("\t", "").Trim();
            line = System.Text.RegularExpressions.Regex.Split(line, "//")[0].Trim();

            if (line.Contains("="))
            {
                string key = line.Split('=')[0].Replace("*", "").ToLower();
                string value = line.Substring(line.IndexOf('=') + 1);
                value = value.Replace("(", "").Replace(")", "").Replace("\"", "").Replace(";", ",").Trim();

                pair = new KeyValuePair<string, string>(key, value);
            }
            else if (line.ElementAt(0) == '[')
            {
                pair = new KeyValuePair<string, string>(line.Trim().ToLower(), "[TITLE]");
            }
            else if (line.Contains(":"))
            {
                pair = new KeyValuePair<string, string>(line.Substring(0, line.IndexOf(':')).Trim().ToLower(), ":SUBCATIGORY:");
            }
            else
            {
                pair = new KeyValuePair<string, string>(line.Trim().ToLower(), null);
            }

            return pair;
        }

        public static Dictionary<string, string> addDefaultValues(Dictionary<string, string> dict, Dictionary<string, string> defaults)
        {
            Dictionary<string, string>.Enumerator enumerator = defaults.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, string> pair = enumerator.Current;

                if (!dict.ContainsKey(pair.Key))
                {
                    dict.Add(pair.Key, pair.Value);
                }
            }

            return dict;
        }

        public static string getHashCode(string path)
        {
            if (!File.Exists(path))
                return null;

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        public static string getRFactor2Loc(string origin)
        {
            //Application is in the rf2 folder
            if (origin != null)
            {
                string folder = origin;
                while (Path.GetDirectoryName(folder).Contains("rFactor 2"))
                    folder = Path.GetDirectoryName(folder);

                if (checkForSoftware(folder))
                    return folder;
            }


            try
            {
                //Steam install
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 365960", false))
                {
                    string folder = "" + key.GetValue("InstallLocation");

                    if (checkForSoftware(folder))
                        return folder;
                }
            }
            catch (NullReferenceException)
            {
                try
                {
                    //Old instal
                    RegistryKey MyReg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rFactor2", false);
                    string folder = "" + MyReg.GetValue("DataPath");

                    if (checkForSoftware(folder))
                        return folder;
                }
                catch (NullReferenceException)
                {

                }
            }

            //CarStat
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + "CarStat", true);

                string folder = MyReg.GetValue("Startupdir-RF2").ToString();

                if (checkForSoftware(folder))
                    return folder;

                folder = MyReg.GetValue("Startupdir").ToString(); //Old CarStat Loc

                if (checkForSoftware(folder))
                    return folder;

            }
            catch (NullReferenceException)
            {

            }

            return null;
        }

        public static bool checkForSoftware(string rf2Folder)
        {
            if (rf2Folder == null)
                return false;

            string modgr = Path.Combine(rf2Folder, "Bin32", "ModMgr.exe");
            string mas2 = Path.Combine(rf2Folder, "Support", "Tools", "MAS2.exe");
            return File.Exists(modgr) && File.Exists(mas2);
        }

        public static string getMASTool(string rf2Folder)
        {
            string mas2 = Path.Combine(rf2Folder, "Support", "Tools", "MAS2.exe");
            if (File.Exists(mas2))
                return mas2;

            return null;
        }

        public static string getModMgr(string rf2Folder)
        {
            string modgr = Path.Combine(rf2Folder, "Bin32", "ModMgr.exe");
            if (File.Exists(modgr))
                return modgr;

            return null;
        }

        public static Process addFilesToMas(string ModMgrPath, string masPath, string[] files)
        {
            string origin = masPath.Replace(Path.GetExtension(masPath), "");
            string spezialFolder = Path.Combine(origin, "temp");
            masPath = Path.Combine(spezialFolder, Path.GetFileName(masPath));

            if (!Directory.Exists(origin))
                Directory.CreateDirectory(origin);

            if (!Directory.Exists(spezialFolder))
                Directory.CreateDirectory(spezialFolder);
            
            for (int i = 0; i < files.Length; i++)
            {
                string dest = Path.Combine(origin, Path.GetFileName(files[i]));

                File.Copy(files[i], dest, true);
            }


            Process pro = new Process();
            ProcessStartInfo info = new ProcessStartInfo(ModMgrPath,
                "\"-m" + masPath + "\"" + " " +
                 ("\""+origin + Path.DirectorySeparatorChar + "*.*"+"\""));
            info.WindowStyle = ProcessWindowStyle.Hidden;
            pro.StartInfo = info;

            pro.Start();

            return pro;
        }

        //This will be the function to do it all
        public static void buildRfcmp(string mas2path, string origin)
        {
            Process process = new Process();
            process.StartInfo.FileName = mas2path;
            process.StartInfo.WorkingDirectory = origin;
            process.Start();
            while (process.MainWindowHandle == (IntPtr)0)
                Thread.Sleep(10);
            AutomationElement mas2Window = GetAutomationElementFromProcessId(process.Id);

            new Thread((a0 => clickButton(mas2Window, a0.ToString()))).Start("Create the package file.");

            while (mas2Window.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, "Create Single Cmp Package")) == null)
                Thread.Sleep(100);
            new Thread((a0 => clickButton(mas2Window, a0.ToString()))).Start("Create Single Cmp Package");
            while (mas2Window.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, "Package")) == null)
                Thread.Sleep(100);
            clickButton(mas2Window, "Package");

            //while (mas2Window.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, "Component packaging complete with no errors")) == null)
            //Thread.Sleep(100);

            Thread.Sleep(1000);


            process.Kill();
        }

        private static AutomationElement GetAutomationElementFromProcessId(int processId)
        {
            AutomationElement element = TreeWalker.RawViewWalker.GetFirstChild(AutomationElement.RootElement);
            while ((object)element != null && element.Current.ProcessId != processId)
                element = TreeWalker.RawViewWalker.GetNextSibling(element);
            return element;
        }

        private static void clickButton(AutomationElement element, string buttonName)
        {
            AutomationElement button = element.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.NameProperty, buttonName));
            ((InvokePattern)button.GetCurrentPattern(InvokePattern.Pattern)).Invoke();
        }

        public static void setMAS2PathForCmppack(string rf2path)
        {
            string path = Path.Combine(rf2path, "Support\\Tools\\mas2.exe");
            if (!File.Exists(path))
                throw new ArgumentException("Path invalid!");

            try
            {
                Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).SetValue("mas2path", path);
            }
            catch
            {
                Registry.CurrentUser.CreateSubKey("Software\\Noel Hibbard\\CmpPack");
                Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).SetValue("defURL", "");
                Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).SetValue("defAuth", "");
                Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).SetValue("defOutPath", "");
                Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).SetValue("relativePaths", "false");

                setMAS2PathForCmppack(rf2path);
            }
            
        }

        //Bodge to get it working
        public static void buildRfcmpWithCmpack(string cmppackPath, string cmpinfo)
        {
            Process[] preProcesses = Process.GetProcessesByName("MAS2");

            Process process = new Process();
            process.StartInfo.FileName = cmppackPath;
            process.StartInfo.Arguments = "--file=\"" + cmpinfo + "\"";
            process.Start();

            while (process.MainWindowHandle == (IntPtr)0)
                Thread.Sleep(10);
            AutomationElement cmppackWindow = GetAutomationElementFromProcessId(process.Id);
            clickButton(cmppackWindow, "Package");

            Thread.Sleep(20);

            int counter = 0;

            Process[] runningProcesses = Process.GetProcessesByName("MAS2");
            while (runningProcesses.Length == preProcesses.Length && counter < 100)
            {
                Thread.Sleep(20);
                runningProcesses = Process.GetProcessesByName("MAS2");
                counter++;
            }
                

            if (runningProcesses.Length > preProcesses.Length)
            {
                Process masPro = null;
                int i = 0;
                for (i = 0; i < preProcesses.Length; i++)
                {
                    if (preProcesses[i] == null && runningProcesses != null)
                        break;
                    else if (runningProcesses == null)
                    {
                    }
                    else if (preProcesses[i].Id != runningProcesses[i].Id)
                    {
                        break;
                    }
                }

                masPro = runningProcesses[i];


                masPro.WaitForExit();
            }
            else
            {
                Thread.Sleep(5000);
            }

            process.Kill();
        }

        public static void extractCmppack(string path)
        {
            if (File.Exists(path))
                return;
           
            File.WriteAllBytes(path, Resources.cmppack);
        }

        public static void deleteCmppack(string path)
        {
            int i = 0;
            while (File.Exists(path) && i < 100)
            {
                try
                {
                    File.Delete(path);
                }
                catch
                {
                    i++;
                    Thread.Sleep(100);
                }
            }
        }

        public static int containsArg(string[] args, string keyword)
        {
            for (int i = 1; i < args.Length; i++)
            {
                if (args[i] != null && args[i].ToLower().Contains(keyword))
                    return i;
            }
            return -1;
        }
    }
}
