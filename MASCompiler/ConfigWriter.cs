﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASCompiler
{
    class ConfigWriter
    {
        private StreamWriter writer;

        public ConfigWriter(string path)
        {
            writer = new StreamWriter(path, false);
        }

        public void writeRfBuild(RfBuild rfBuild)
        {
            writer.WriteLine("buildpath=" + rfBuild.buildPath);
            writer.WriteLine("");

            writer.WriteLine("rfcmp");
            writer.WriteLine("{");
            writer.WriteLine("[Component]");
            writer.WriteLine("Name=" + rfBuild.cmpinfo["name"]);
            writer.WriteLine("Type=" + rfBuild.cmpinfo["type"]);
            writer.WriteLine("Version=" + rfBuild.cmpinfo["version"]);
            writer.WriteLine("Author=" + rfBuild.cmpinfo["author"]);
            writer.WriteLine("URL=" + rfBuild.cmpinfo["url"]);
            writer.WriteLine("Desc=" + rfBuild.cmpinfo["desc"]);
            writer.WriteLine("Category=" + rfBuild.cmpinfo["category"]);
            writer.WriteLine("Origin=" + rfBuild.cmpinfo["origin"]);
            writer.WriteLine("}");

            writer.WriteLine("");

            foreach (MASConfig config in rfBuild.MASConfigs)
            {
                writer.WriteLine(config.MasLocation);
                writer.WriteLine("{");

                foreach (string rule in config.rules)
                    writer.WriteLine(rule);

                writer.WriteLine("}");
                writer.WriteLine("");
            }
        }

        public void close()
        {
            writer.Flush();
            writer.Close();
        }
    }
}
