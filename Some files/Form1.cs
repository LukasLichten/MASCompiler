﻿// Decompiled with JetBrains decompiler
// Type: Component_Builder.Form1
// Assembly: cmppack, Version=1.0.0.5, Culture=neutral, PublicKeyToken=null
// MVID: 0B2D0C40-FC4B-4FB0-AFB9-89EDC93507F9
// Assembly location: C:\Git\MASCompiler\Some files\cmppack.exe

using Component_Builder.FolderSelect;
using Component_Builder.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Windows.Input;

namespace Component_Builder
{
  [DesignerGenerated]
  public class Form1 : Form
  {
    private IContainer components;
    [AccessedThroughProperty("bOpen")]
    private Button _bOpen;
    [AccessedThroughProperty("tbName")]
    private TextBox _tbName;
    [AccessedThroughProperty("lName")]
    private Label _lName;
    [AccessedThroughProperty("cbType")]
    private ComboBox _cbType;
    [AccessedThroughProperty("Label2")]
    private Label _Label2;
    [AccessedThroughProperty("Label3")]
    private Label _Label3;
    [AccessedThroughProperty("tbVer")]
    private TextBox _tbVer;
    [AccessedThroughProperty("Label4")]
    private Label _Label4;
    [AccessedThroughProperty("cbOrig")]
    private ComboBox _cbOrig;
    [AccessedThroughProperty("Label5")]
    private Label _Label5;
    [AccessedThroughProperty("cbCat")]
    private ComboBox _cbCat;
    [AccessedThroughProperty("Label6")]
    private Label _Label6;
    [AccessedThroughProperty("tbAuthor")]
    private TextBox _tbAuthor;
    [AccessedThroughProperty("Label7")]
    private Label _Label7;
    [AccessedThroughProperty("tbDate")]
    private TextBox _tbDate;
    [AccessedThroughProperty("Label8")]
    private Label _Label8;
    [AccessedThroughProperty("tbURL")]
    private TextBox _tbURL;
    [AccessedThroughProperty("Label9")]
    private Label _Label9;
    [AccessedThroughProperty("tbDesc")]
    private TextBox _tbDesc;
    [AccessedThroughProperty("bMasFiles")]
    private Button _bMasFiles;
    [AccessedThroughProperty("lMasCount")]
    private Label _lMasCount;
    [AccessedThroughProperty("bSave")]
    private Button _bSave;
    [AccessedThroughProperty("bPackage")]
    private Button _bPackage;
    [AccessedThroughProperty("bBrowse")]
    private Button _bBrowse;
    [AccessedThroughProperty("tbRfcmpPath")]
    private TextBox _tbRfcmpPath;
    [AccessedThroughProperty("Label1")]
    private Label _Label1;
    [AccessedThroughProperty("Button1")]
    private Button _Button1;
    [AccessedThroughProperty("bDefAuth")]
    private Button _bDefAuth;
    [AccessedThroughProperty("bDefURL")]
    private Button _bDefURL;
    [AccessedThroughProperty("bNew")]
    private Button _bNew;
    [AccessedThroughProperty("bSaveAs")]
    private Button _bSaveAs;
    [AccessedThroughProperty("bNow")]
    private Button _bNow;
    public string cmpInfoPath;
    public Collection masFilesCol;
    public string cmdlineVersion;
    public bool cmdlineBuild;
    public Hashtable OriginTypes;
    public Hashtable VehCategoryTypes;
    public Hashtable LocCategoryTypes;
    public Hashtable OtherCategoryTypes;
    public Hashtable TypeTypes;
    public string defOutPath;
    public string defAuthor;
    public string defURL;
    public bool relativePaths;
    public AutomationElement mas2Window;
    public object mas2Path;

    internal virtual Button bOpen
    {
      get
      {
        return this._bOpen;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bOpen_Click);
        if (this._bOpen != null)
          this._bOpen.Click -= eventHandler;
        this._bOpen = value;
        if (this._bOpen == null)
          return;
        this._bOpen.Click += eventHandler;
      }
    }

    internal virtual TextBox tbName
    {
      get
      {
        return this._tbName;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbName = value;
      }
    }

    internal virtual Label lName
    {
      get
      {
        return this._lName;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._lName = value;
      }
    }

    internal virtual ComboBox cbType
    {
      get
      {
        return this._cbType;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.ComboBox1_SelectedIndexChanged);
        if (this._cbType != null)
          this._cbType.SelectedIndexChanged -= eventHandler;
        this._cbType = value;
        if (this._cbType == null)
          return;
        this._cbType.SelectedIndexChanged += eventHandler;
      }
    }

    internal virtual Label Label2
    {
      get
      {
        return this._Label2;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label2 = value;
      }
    }

    internal virtual Label Label3
    {
      get
      {
        return this._Label3;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label3 = value;
      }
    }

    internal virtual TextBox tbVer
    {
      get
      {
        return this._tbVer;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbVer = value;
      }
    }

    internal virtual Label Label4
    {
      get
      {
        return this._Label4;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label4 = value;
      }
    }

    internal virtual ComboBox cbOrig
    {
      get
      {
        return this._cbOrig;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._cbOrig = value;
      }
    }

    internal virtual Label Label5
    {
      get
      {
        return this._Label5;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label5 = value;
      }
    }

    internal virtual ComboBox cbCat
    {
      get
      {
        return this._cbCat;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._cbCat = value;
      }
    }

    internal virtual Label Label6
    {
      get
      {
        return this._Label6;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label6 = value;
      }
    }

    internal virtual TextBox tbAuthor
    {
      get
      {
        return this._tbAuthor;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbAuthor = value;
      }
    }

    internal virtual Label Label7
    {
      get
      {
        return this._Label7;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label7 = value;
      }
    }

    internal virtual TextBox tbDate
    {
      get
      {
        return this._tbDate;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbDate = value;
      }
    }

    internal virtual Label Label8
    {
      get
      {
        return this._Label8;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label8 = value;
      }
    }

    internal virtual TextBox tbURL
    {
      get
      {
        return this._tbURL;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbURL = value;
      }
    }

    internal virtual Label Label9
    {
      get
      {
        return this._Label9;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label9 = value;
      }
    }

    internal virtual TextBox tbDesc
    {
      get
      {
        return this._tbDesc;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbDesc = value;
      }
    }

    internal virtual Button bMasFiles
    {
      get
      {
        return this._bMasFiles;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bMasFiles_Click);
        if (this._bMasFiles != null)
          this._bMasFiles.Click -= eventHandler;
        this._bMasFiles = value;
        if (this._bMasFiles == null)
          return;
        this._bMasFiles.Click += eventHandler;
      }
    }

    internal virtual Label lMasCount
    {
      get
      {
        return this._lMasCount;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._lMasCount = value;
      }
    }

    internal virtual Button bSave
    {
      get
      {
        return this._bSave;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bSave_Click);
        if (this._bSave != null)
          this._bSave.Click -= eventHandler;
        this._bSave = value;
        if (this._bSave == null)
          return;
        this._bSave.Click += eventHandler;
      }
    }

    internal virtual Button bPackage
    {
      get
      {
        return this._bPackage;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bPackage_Click);
        if (this._bPackage != null)
          this._bPackage.Click -= eventHandler;
        this._bPackage = value;
        if (this._bPackage == null)
          return;
        this._bPackage.Click += eventHandler;
      }
    }

    internal virtual Button bBrowse
    {
      get
      {
        return this._bBrowse;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bBrowse_Click);
        if (this._bBrowse != null)
          this._bBrowse.Click -= eventHandler;
        this._bBrowse = value;
        if (this._bBrowse == null)
          return;
        this._bBrowse.Click += eventHandler;
      }
    }

    internal virtual TextBox tbRfcmpPath
    {
      get
      {
        return this._tbRfcmpPath;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._tbRfcmpPath = value;
      }
    }

    internal virtual Label Label1
    {
      get
      {
        return this._Label1;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        this._Label1 = value;
      }
    }

    internal virtual Button Button1
    {
      get
      {
        return this._Button1;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.Button1_Click);
        if (this._Button1 != null)
          this._Button1.Click -= eventHandler;
        this._Button1 = value;
        if (this._Button1 == null)
          return;
        this._Button1.Click += eventHandler;
      }
    }

    internal virtual Button bDefAuth
    {
      get
      {
        return this._bDefAuth;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bDefAuth_Click);
        if (this._bDefAuth != null)
          this._bDefAuth.Click -= eventHandler;
        this._bDefAuth = value;
        if (this._bDefAuth == null)
          return;
        this._bDefAuth.Click += eventHandler;
      }
    }

    internal virtual Button bDefURL
    {
      get
      {
        return this._bDefURL;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bDefURL_Click);
        if (this._bDefURL != null)
          this._bDefURL.Click -= eventHandler;
        this._bDefURL = value;
        if (this._bDefURL == null)
          return;
        this._bDefURL.Click += eventHandler;
      }
    }

    internal virtual Button bNew
    {
      get
      {
        return this._bNew;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bNew_Click);
        if (this._bNew != null)
          this._bNew.Click -= eventHandler;
        this._bNew = value;
        if (this._bNew == null)
          return;
        this._bNew.Click += eventHandler;
      }
    }

    internal virtual Button bSaveAs
    {
      get
      {
        return this._bSaveAs;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bSaveAs_Click);
        if (this._bSaveAs != null)
          this._bSaveAs.Click -= eventHandler;
        this._bSaveAs = value;
        if (this._bSaveAs == null)
          return;
        this._bSaveAs.Click += eventHandler;
      }
    }

    internal virtual Button bNow
    {
      get
      {
        return this._bNow;
      }
      [MethodImpl(MethodImplOptions.Synchronized)] set
      {
        EventHandler eventHandler = new EventHandler(this.bNow_Click);
        if (this._bNow != null)
          this._bNow.Click -= eventHandler;
        this._bNow = value;
        if (this._bNow == null)
          return;
        this._bNow.Click += eventHandler;
      }
    }

    public Form1()
    {
      this.Load += new EventHandler(this.Form1_Load);
      this.cmpInfoPath = "";
      this.masFilesCol = new Collection();
      this.cmdlineVersion = "";
      this.cmdlineBuild = false;
      this.OriginTypes = new Hashtable();
      this.VehCategoryTypes = new Hashtable();
      this.LocCategoryTypes = new Hashtable();
      this.OtherCategoryTypes = new Hashtable();
      this.TypeTypes = new Hashtable();
      this.defOutPath = "";
      this.defAuthor = "";
      this.defURL = "";
      this.relativePaths = false;
      this.mas2Path = (object) "";
      this.InitializeComponent();
    }

    [DebuggerNonUserCode]
    protected override void Dispose(bool disposing)
    {
      try
      {
        if (!disposing || this.components == null)
          return;
        this.components.Dispose();
      }
      finally
      {
        base.Dispose(disposing);
      }
    }

    [DebuggerStepThrough]
    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Form1));
      this.bOpen = new Button();
      this.tbName = new TextBox();
      this.lName = new Label();
      this.cbType = new ComboBox();
      this.Label2 = new Label();
      this.Label3 = new Label();
      this.tbVer = new TextBox();
      this.Label4 = new Label();
      this.cbOrig = new ComboBox();
      this.Label5 = new Label();
      this.cbCat = new ComboBox();
      this.Label6 = new Label();
      this.tbAuthor = new TextBox();
      this.Label7 = new Label();
      this.tbDate = new TextBox();
      this.Label8 = new Label();
      this.tbURL = new TextBox();
      this.Label9 = new Label();
      this.tbDesc = new TextBox();
      this.bMasFiles = new Button();
      this.lMasCount = new Label();
      this.bSave = new Button();
      this.bPackage = new Button();
      this.bBrowse = new Button();
      this.tbRfcmpPath = new TextBox();
      this.Label1 = new Label();
      this.Button1 = new Button();
      this.bDefAuth = new Button();
      this.bDefURL = new Button();
      this.bNew = new Button();
      this.bSaveAs = new Button();
      this.bNow = new Button();
      this.SuspendLayout();
      this.bOpen.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      Button bOpen1 = this.bOpen;
      Point point1 = new Point(93, 394);
      Point point2 = point1;
      bOpen1.Location = point2;
      this.bOpen.Name = "bOpen";
      Button bOpen2 = this.bOpen;
      Size size1 = new Size(75, 23);
      Size size2 = size1;
      bOpen2.Size = size2;
      this.bOpen.TabIndex = 0;
      this.bOpen.Text = "Open";
      this.bOpen.UseVisualStyleBackColor = true;
      TextBox tbName1 = this.tbName;
      point1 = new Point(76, 12);
      Point point3 = point1;
      tbName1.Location = point3;
      this.tbName.Name = "tbName";
      TextBox tbName2 = this.tbName;
      size1 = new Size(186, 20);
      Size size3 = size1;
      tbName2.Size = size3;
      this.tbName.TabIndex = 1;
      this.lName.AutoSize = true;
      Label lName1 = this.lName;
      point1 = new Point(10, 15);
      Point point4 = point1;
      lName1.Location = point4;
      this.lName.Name = "lName";
      Label lName2 = this.lName;
      size1 = new Size(35, 13);
      Size size4 = size1;
      lName2.Size = size4;
      this.lName.TabIndex = 2;
      this.lName.Text = "Name";
      this.cbType.DropDownStyle = ComboBoxStyle.DropDownList;
      this.cbType.FormattingEnabled = true;
      this.cbType.Items.AddRange(new object[12]
      {
        (object) "Location",
        (object) "Vehicle",
        (object) "Sounds",
        (object) "HUD",
        (object) "Scripts",
        (object) "Cometary",
        (object) "Talent",
        (object) "Helmets",
        (object) "Nations",
        (object) "Showroom",
        (object) "UI",
        (object) "Other"
      });
      ComboBox cbType1 = this.cbType;
      point1 = new Point(76, 38);
      Point point5 = point1;
      cbType1.Location = point5;
      this.cbType.Name = "cbType";
      ComboBox cbType2 = this.cbType;
      size1 = new Size(186, 21);
      Size size5 = size1;
      cbType2.Size = size5;
      this.cbType.TabIndex = 3;
      this.Label2.AutoSize = true;
      Label label2_1 = this.Label2;
      point1 = new Point(10, 41);
      Point point6 = point1;
      label2_1.Location = point6;
      this.Label2.Name = "Label2";
      Label label2_2 = this.Label2;
      size1 = new Size(31, 13);
      Size size6 = size1;
      label2_2.Size = size6;
      this.Label2.TabIndex = 4;
      this.Label2.Text = "Type";
      this.Label3.AutoSize = true;
      Label label3_1 = this.Label3;
      point1 = new Point(268, 15);
      Point point7 = point1;
      label3_1.Location = point7;
      this.Label3.Name = "Label3";
      Label label3_2 = this.Label3;
      size1 = new Size(42, 13);
      Size size7 = size1;
      label3_2.Size = size7;
      this.Label3.TabIndex = 5;
      this.Label3.Text = "Version";
      TextBox tbVer1 = this.tbVer;
      point1 = new Point(314, 11);
      Point point8 = point1;
      tbVer1.Location = point8;
      this.tbVer.Name = "tbVer";
      TextBox tbVer2 = this.tbVer;
      size1 = new Size(100, 20);
      Size size8 = size1;
      tbVer2.Size = size8;
      this.tbVer.TabIndex = 6;
      this.Label4.AutoSize = true;
      Label label4_1 = this.Label4;
      point1 = new Point(268, 41);
      Point point9 = point1;
      label4_1.Location = point9;
      this.Label4.Name = "Label4";
      Label label4_2 = this.Label4;
      size1 = new Size(34, 13);
      Size size9 = size1;
      label4_2.Size = size9;
      this.Label4.TabIndex = 8;
      this.Label4.Text = "Origin";
      this.cbOrig.DropDownStyle = ComboBoxStyle.DropDownList;
      this.cbOrig.FormattingEnabled = true;
      this.cbOrig.Items.AddRange(new object[3]
      {
        (object) "Scratch",
        (object) "Conversion",
        (object) "Composite"
      });
      ComboBox cbOrig1 = this.cbOrig;
      point1 = new Point(314, 37);
      Point point10 = point1;
      cbOrig1.Location = point10;
      this.cbOrig.Name = "cbOrig";
      ComboBox cbOrig2 = this.cbOrig;
      size1 = new Size(121, 21);
      Size size10 = size1;
      cbOrig2.Size = size10;
      this.cbOrig.TabIndex = 7;
      this.Label5.AutoSize = true;
      Label label5_1 = this.Label5;
      point1 = new Point(10, 68);
      Point point11 = point1;
      label5_1.Location = point11;
      this.Label5.Name = "Label5";
      Label label5_2 = this.Label5;
      size1 = new Size(49, 13);
      Size size11 = size1;
      label5_2.Size = size11;
      this.Label5.TabIndex = 10;
      this.Label5.Text = "Catagory";
      this.cbCat.DropDownStyle = ComboBoxStyle.DropDownList;
      this.cbCat.FormattingEnabled = true;
      ComboBox cbCat1 = this.cbCat;
      point1 = new Point(76, 65);
      Point point12 = point1;
      cbCat1.Location = point12;
      this.cbCat.Name = "cbCat";
      ComboBox cbCat2 = this.cbCat;
      size1 = new Size(186, 21);
      Size size12 = size1;
      cbCat2.Size = size12;
      this.cbCat.TabIndex = 9;
      this.Label6.AutoSize = true;
      Label label6_1 = this.Label6;
      point1 = new Point(10, 123);
      Point point13 = point1;
      label6_1.Location = point13;
      this.Label6.Name = "Label6";
      Label label6_2 = this.Label6;
      size1 = new Size(38, 13);
      Size size13 = size1;
      label6_2.Size = size13;
      this.Label6.TabIndex = 12;
      this.Label6.Text = "Author";
      TextBox tbAuthor1 = this.tbAuthor;
      point1 = new Point(76, 120);
      Point point14 = point1;
      tbAuthor1.Location = point14;
      this.tbAuthor.Name = "tbAuthor";
      TextBox tbAuthor2 = this.tbAuthor;
      size1 = new Size(350, 20);
      Size size14 = size1;
      tbAuthor2.Size = size14;
      this.tbAuthor.TabIndex = 11;
      this.Label7.AutoSize = true;
      Label label7_1 = this.Label7;
      point1 = new Point(9, 95);
      Point point15 = point1;
      label7_1.Location = point15;
      this.Label7.Name = "Label7";
      Label label7_2 = this.Label7;
      size1 = new Size(30, 13);
      Size size15 = size1;
      label7_2.Size = size15;
      this.Label7.TabIndex = 14;
      this.Label7.Text = "Date";
      TextBox tbDate1 = this.tbDate;
      point1 = new Point(76, 92);
      Point point16 = point1;
      tbDate1.Location = point16;
      this.tbDate.Name = "tbDate";
      TextBox tbDate2 = this.tbDate;
      size1 = new Size(186, 20);
      Size size16 = size1;
      tbDate2.Size = size16;
      this.tbDate.TabIndex = 13;
      this.Label8.AutoSize = true;
      Label label8_1 = this.Label8;
      point1 = new Point(9, 149);
      Point point17 = point1;
      label8_1.Location = point17;
      this.Label8.Name = "Label8";
      Label label8_2 = this.Label8;
      size1 = new Size(29, 13);
      Size size17 = size1;
      label8_2.Size = size17;
      this.Label8.TabIndex = 16;
      this.Label8.Text = "URL";
      TextBox tbUrl1 = this.tbURL;
      point1 = new Point(76, 146);
      Point point18 = point1;
      tbUrl1.Location = point18;
      this.tbURL.Name = "tbURL";
      TextBox tbUrl2 = this.tbURL;
      size1 = new Size(350, 20);
      Size size18 = size1;
      tbUrl2.Size = size18;
      this.tbURL.TabIndex = 15;
      this.Label9.AutoSize = true;
      Label label9_1 = this.Label9;
      point1 = new Point(9, 175);
      Point point19 = point1;
      label9_1.Location = point19;
      this.Label9.Name = "Label9";
      Label label9_2 = this.Label9;
      size1 = new Size(60, 13);
      Size size19 = size1;
      label9_2.Size = size19;
      this.Label9.TabIndex = 18;
      this.Label9.Text = "Description";
      this.tbDesc.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      TextBox tbDesc1 = this.tbDesc;
      point1 = new Point(76, 172);
      Point point20 = point1;
      tbDesc1.Location = point20;
      this.tbDesc.Multiline = true;
      this.tbDesc.Name = "tbDesc";
      TextBox tbDesc2 = this.tbDesc;
      size1 = new Size(431, 174);
      Size size20 = size1;
      tbDesc2.Size = size20;
      this.tbDesc.TabIndex = 17;
      this.bMasFiles.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      Button bMasFiles1 = this.bMasFiles;
      point1 = new Point(432, 89);
      Point point21 = point1;
      bMasFiles1.Location = point21;
      this.bMasFiles.Name = "bMasFiles";
      Button bMasFiles2 = this.bMasFiles;
      size1 = new Size(75, 23);
      Size size21 = size1;
      bMasFiles2.Size = size21;
      this.bMasFiles.TabIndex = 19;
      this.bMasFiles.Text = "MAS Files";
      this.bMasFiles.UseVisualStyleBackColor = true;
      this.lMasCount.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.lMasCount.AutoSize = true;
      Label lMasCount1 = this.lMasCount;
      point1 = new Point(352, 94);
      Point point22 = point1;
      lMasCount1.Location = point22;
      this.lMasCount.Name = "lMasCount";
      Label lMasCount2 = this.lMasCount;
      size1 = new Size(66, 13);
      Size size22 = size1;
      lMasCount2.Size = size22;
      this.lMasCount.TabIndex = 20;
      this.lMasCount.Text = "MAS Files: 0";
      this.bSave.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      Button bSave1 = this.bSave;
      point1 = new Point(174, 394);
      Point point23 = point1;
      bSave1.Location = point23;
      this.bSave.Name = "bSave";
      Button bSave2 = this.bSave;
      size1 = new Size(75, 23);
      Size size23 = size1;
      bSave2.Size = size23;
      this.bSave.TabIndex = 21;
      this.bSave.Text = "Save";
      this.bSave.UseVisualStyleBackColor = true;
      this.bPackage.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      Button bPackage1 = this.bPackage;
      point1 = new Point(432, 394);
      Point point24 = point1;
      bPackage1.Location = point24;
      this.bPackage.Name = "bPackage";
      Button bPackage2 = this.bPackage;
      size1 = new Size(75, 23);
      Size size24 = size1;
      bPackage2.Size = size24;
      this.bPackage.TabIndex = 22;
      this.bPackage.Text = "Package";
      this.bPackage.UseVisualStyleBackColor = true;
      this.bBrowse.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      Button bBrowse1 = this.bBrowse;
      point1 = new Point(432, 365);
      Point point25 = point1;
      bBrowse1.Location = point25;
      this.bBrowse.Name = "bBrowse";
      Button bBrowse2 = this.bBrowse;
      size1 = new Size(75, 23);
      Size size25 = size1;
      bBrowse2.Size = size25;
      this.bBrowse.TabIndex = 23;
      this.bBrowse.Text = "Browse";
      this.bBrowse.UseVisualStyleBackColor = true;
      this.tbRfcmpPath.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      TextBox tbRfcmpPath1 = this.tbRfcmpPath;
      point1 = new Point(87, 367);
      Point point26 = point1;
      tbRfcmpPath1.Location = point26;
      this.tbRfcmpPath.Name = "tbRfcmpPath";
      TextBox tbRfcmpPath2 = this.tbRfcmpPath;
      size1 = new Size(339, 20);
      Size size26 = size1;
      tbRfcmpPath2.Size = size26;
      this.tbRfcmpPath.TabIndex = 24;
      this.Label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.Label1.AutoSize = true;
      Label label1_1 = this.Label1;
      point1 = new Point(10, 370);
      Point point27 = point1;
      label1_1.Location = point27;
      this.Label1.Name = "Label1";
      Label label1_2 = this.Label1;
      size1 = new Size(71, 13);
      Size size27 = size1;
      label1_2.Size = size27;
      this.Label1.TabIndex = 25;
      this.Label1.Text = "Output Folder";
      this.Button1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      Button button1_1 = this.Button1;
      point1 = new Point(432, 10);
      Point point28 = point1;
      button1_1.Location = point28;
      this.Button1.Name = "Button1";
      Button button1_2 = this.Button1;
      size1 = new Size(75, 23);
      Size size28 = size1;
      button1_2.Size = size28;
      this.Button1.TabIndex = 26;
      this.Button1.Text = "Settings";
      this.Button1.UseVisualStyleBackColor = true;
      this.bDefAuth.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      Button bDefAuth1 = this.bDefAuth;
      point1 = new Point(432, 118);
      Point point29 = point1;
      bDefAuth1.Location = point29;
      this.bDefAuth.Name = "bDefAuth";
      Button bDefAuth2 = this.bDefAuth;
      size1 = new Size(75, 23);
      Size size29 = size1;
      bDefAuth2.Size = size29;
      this.bDefAuth.TabIndex = 27;
      this.bDefAuth.Text = "Default";
      this.bDefAuth.UseVisualStyleBackColor = true;
      this.bDefURL.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      Button bDefUrl1 = this.bDefURL;
      point1 = new Point(432, 144);
      Point point30 = point1;
      bDefUrl1.Location = point30;
      this.bDefURL.Name = "bDefURL";
      Button bDefUrl2 = this.bDefURL;
      size1 = new Size(75, 23);
      Size size30 = size1;
      bDefUrl2.Size = size30;
      this.bDefURL.TabIndex = 28;
      this.bDefURL.Text = "Default";
      this.bDefURL.UseVisualStyleBackColor = true;
      this.bNew.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      Button bNew1 = this.bNew;
      point1 = new Point(12, 394);
      Point point31 = point1;
      bNew1.Location = point31;
      this.bNew.Name = "bNew";
      Button bNew2 = this.bNew;
      size1 = new Size(75, 23);
      Size size31 = size1;
      bNew2.Size = size31;
      this.bNew.TabIndex = 29;
      this.bNew.Text = "New";
      this.bNew.UseVisualStyleBackColor = true;
      this.bSaveAs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      Button bSaveAs1 = this.bSaveAs;
      point1 = new Point((int) byte.MaxValue, 393);
      Point point32 = point1;
      bSaveAs1.Location = point32;
      this.bSaveAs.Name = "bSaveAs";
      Button bSaveAs2 = this.bSaveAs;
      size1 = new Size(75, 23);
      Size size32 = size1;
      bSaveAs2.Size = size32;
      this.bSaveAs.TabIndex = 30;
      this.bSaveAs.Text = "Save As";
      this.bSaveAs.UseVisualStyleBackColor = true;
      Button bNow1 = this.bNow;
      point1 = new Point(268, 90);
      Point point33 = point1;
      bNow1.Location = point33;
      this.bNow.Name = "bNow";
      Button bNow2 = this.bNow;
      size1 = new Size(50, 23);
      Size size33 = size1;
      bNow2.Size = size33;
      this.bNow.TabIndex = 31;
      this.bNow.Text = "Now";
      this.bNow.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      size1 = new Size(519, 429);
      this.ClientSize = size1;
      this.Controls.Add((Control) this.bNow);
      this.Controls.Add((Control) this.bSaveAs);
      this.Controls.Add((Control) this.bNew);
      this.Controls.Add((Control) this.bDefURL);
      this.Controls.Add((Control) this.bDefAuth);
      this.Controls.Add((Control) this.Button1);
      this.Controls.Add((Control) this.Label1);
      this.Controls.Add((Control) this.tbRfcmpPath);
      this.Controls.Add((Control) this.bBrowse);
      this.Controls.Add((Control) this.bPackage);
      this.Controls.Add((Control) this.bSave);
      this.Controls.Add((Control) this.lMasCount);
      this.Controls.Add((Control) this.bMasFiles);
      this.Controls.Add((Control) this.Label9);
      this.Controls.Add((Control) this.tbDesc);
      this.Controls.Add((Control) this.Label8);
      this.Controls.Add((Control) this.tbURL);
      this.Controls.Add((Control) this.Label7);
      this.Controls.Add((Control) this.tbDate);
      this.Controls.Add((Control) this.Label6);
      this.Controls.Add((Control) this.tbAuthor);
      this.Controls.Add((Control) this.Label5);
      this.Controls.Add((Control) this.cbCat);
      this.Controls.Add((Control) this.Label4);
      this.Controls.Add((Control) this.cbOrig);
      this.Controls.Add((Control) this.tbVer);
      this.Controls.Add((Control) this.Label3);
      this.Controls.Add((Control) this.Label2);
      this.Controls.Add((Control) this.cbType);
      this.Controls.Add((Control) this.lName);
      this.Controls.Add((Control) this.tbName);
      this.Controls.Add((Control) this.bOpen);
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = "Form1";
      this.Text = "Component Builder";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void bMasFiles_Click(object sender, EventArgs e)
    {
      MyProject.Forms.MasFiles.Show();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      if (Registry.CurrentUser.OpenSubKey("SOFTWARE\\Noel Hibbard\\CmpPack") == null)
      {
        Registry.CurrentUser.CreateSubKey("SOFTWARE\\Noel Hibbard\\CmpPack");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\""));
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\" --package"));
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\",0"));
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\.cmpinfo");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\.mft");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\.cmpinfo", true).SetValue("", (object) "rF2CmpPack");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\.mft", true).SetValue("", (object) "rF2CmpPack");
        if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists("mas2.exe"))
          this.mas2Path = (object) (Environment.CurrentDirectory + "\\mas2.exe");
        if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists("data.path"))
          this.mas2Path = (object) (Microsoft.VisualBasic.FileIO.FileSystem.ReadAllText("data.path").Replace("/", "\\") + "\\Support\\Tools\\mas2.exe");
        int num1 = (int) Interaction.MsgBox((object) "This is your first time running this tool so you need to set the location of the MAS2 Tool.", MsgBoxStyle.OkOnly, (object) null);
        int num2 = (int) MyProject.Forms.dSettings.ShowDialog();
      }
      try
      {
        if (!Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command", true).GetValue("").ToString().Contains(Environment.GetCommandLineArgs()[0]))
        {
          Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command");
          Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command");
          Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon");
          Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\""));
          Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\" --package"));
          Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\",0"));
          Registry.CurrentUser.CreateSubKey("Software\\Classes\\.cmpinfo");
          Registry.CurrentUser.CreateSubKey("Software\\Classes\\.mft");
          Registry.CurrentUser.OpenSubKey("Software\\Classes\\.cmpinfo", true).SetValue("", (object) "rF2CmpPack");
          Registry.CurrentUser.OpenSubKey("Software\\Classes\\.mft", true).SetValue("", (object) "rF2CmpPack");
        }
      }
      catch (Exception ex)
      {
        ProjectData.SetProjectError(ex);
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\open\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\""));
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\shell\\Package\\command", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\" --file=\"%1\" --package"));
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\rF2CmpPack\\DefaultIcon", true).SetValue("", (object) ("\"" + Environment.GetCommandLineArgs()[0] + "\",0"));
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\.cmpinfo");
        Registry.CurrentUser.CreateSubKey("Software\\Classes\\.mft");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\.cmpinfo", true).SetValue("", (object) "rF2CmpPack");
        Registry.CurrentUser.OpenSubKey("Software\\Classes\\.mft", true).SetValue("", (object) "rF2CmpPack");
        ProjectData.ClearProjectError();
      }
      this.defURL = Conversions.ToString(Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).GetValue("defURL"));
      this.defAuthor = Conversions.ToString(Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).GetValue("defAuth"));
      this.defOutPath = Conversions.ToString(Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).GetValue("defOutPath"));
      this.relativePaths = Conversions.ToBoolean(Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).GetValue("relativePaths"));
      if (((IEnumerable<string>) Environment.GetCommandLineArgs()).Count<string>() > 1)
      {
        string[] commandLineArgs = Environment.GetCommandLineArgs();
        int index = 0;
        while (index < commandLineArgs.Length)
        {
          string str = commandLineArgs[index];
          if (str.ToLower().StartsWith("--file"))
            this.cmpInfoPath = str.Split('=')[1];
          if (str.ToLower().StartsWith("--version"))
            this.cmdlineVersion = str.Split('=')[1];
          if (str.ToLower().StartsWith("--package"))
            this.cmdlineBuild = true;
          if (str.ToLower().StartsWith("--help"))
          {
            Console.WriteLine("--file=<filepath> [--version=<version>] [--package]");
            this.Close();
          }
          checked { ++index; }
        }
      }
      this.mas2Path = RuntimeHelpers.GetObjectValue(Registry.CurrentUser.OpenSubKey("Software\\Noel Hibbard\\CmpPack", true).GetValue("mas2path"));
      this.OriginTypes.Add((object) "", (object) 0);
      this.OriginTypes.Add((object) "Unknown", (object) 0);
      this.OriginTypes.Add((object) "Scratch", (object) 1);
      this.OriginTypes.Add((object) "Conversion", (object) 2);
      this.OriginTypes.Add((object) "Composite", (object) 3);
      this.VehCategoryTypes.Add((object) "", (object) 0);
      this.VehCategoryTypes.Add((object) "Unknown", (object) 0);
      this.VehCategoryTypes.Add((object) "Dirt Oval", (object) 1);
      this.VehCategoryTypes.Add((object) "Dirt Raly", (object) 2);
      this.VehCategoryTypes.Add((object) "GT", (object) 3);
      this.VehCategoryTypes.Add((object) "Karts", (object) 4);
      this.VehCategoryTypes.Add((object) "Novel", (object) 5);
      this.VehCategoryTypes.Add((object) "Open Wheeler", (object) 6);
      this.VehCategoryTypes.Add((object) "Prototype", (object) 7);
      this.VehCategoryTypes.Add((object) "Stock Car", (object) 8);
      this.VehCategoryTypes.Add((object) "Street Car", (object) 9);
      this.VehCategoryTypes.Add((object) "Touring Car", (object) 10);
      this.LocCategoryTypes.Add((object) "Dirt Oval", (object) 50);
      this.LocCategoryTypes.Add((object) "Dirt Track", (object) 51);
      this.LocCategoryTypes.Add((object) "Kart Track", (object) 52);
      this.LocCategoryTypes.Add((object) "Novel", (object) 53);
      this.LocCategoryTypes.Add((object) "Oval", (object) 54);
      this.LocCategoryTypes.Add((object) "Permanent", (object) 55);
      this.LocCategoryTypes.Add((object) "Raly Cross", (object) 56);
      this.LocCategoryTypes.Add((object) "Temporary", (object) 57);
      this.TypeTypes.Add((object) "Location", (object) 1);
      this.TypeTypes.Add((object) "Vehicle", (object) 2);
      this.TypeTypes.Add((object) "Sounds", (object) 3);
      this.TypeTypes.Add((object) "HUD", (object) 4);
      this.TypeTypes.Add((object) "Scripts", (object) 5);
      this.TypeTypes.Add((object) "Cometary", (object) 6);
      this.TypeTypes.Add((object) "Talent", (object) 7);
      this.TypeTypes.Add((object) "Helmets", (object) 8);
      this.TypeTypes.Add((object) "Nations", (object) 9);
      this.TypeTypes.Add((object) "Showroom", (object) 10);
      this.TypeTypes.Add((object) "UI", (object) 11);
      this.TypeTypes.Add((object) "Other", (object) 12);
      this.lMasCount.Text = "MAS Files: " + Conversions.ToString(this.masFilesCol.Count);
      this.tbDate.Text = Conversions.ToString(DateTime.Now);
      if (Operators.CompareString(this.cmpInfoPath, "", false) == 0)
        return;
      this.openCmpFile();
      if (!this.cmdlineBuild)
        return;
      this.fullPackage();
    }

    private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.cbCat.Items.Clear();
      string text = this.cbType.Text;
      if (Operators.CompareString(text, "Location", false) == 0)
      {
        this.cbCat.Items.Add((object) "Dirt Oval");
        this.cbCat.Items.Add((object) "Dirt Track");
        this.cbCat.Items.Add((object) "Kart Track");
        this.cbCat.Items.Add((object) "Novel");
        this.cbCat.Items.Add((object) "Oval");
        this.cbCat.Items.Add((object) "Permanent");
        this.cbCat.Items.Add((object) "Raly Cross");
        this.cbCat.Items.Add((object) "Temporary");
      }
      else if (Operators.CompareString(text, "Vehicle", false) == 0)
      {
        this.cbCat.Items.Add((object) "Dirt Oval");
        this.cbCat.Items.Add((object) "Dirt Raly");
        this.cbCat.Items.Add((object) "GT");
        this.cbCat.Items.Add((object) "Karts");
        this.cbCat.Items.Add((object) "Novel");
        this.cbCat.Items.Add((object) "Open Wheeler");
        this.cbCat.Items.Add((object) "Prototype");
        this.cbCat.Items.Add((object) "Stock Car");
        this.cbCat.Items.Add((object) "Street Car");
        this.cbCat.Items.Add((object) "Touring Car");
      }
      else if (Operators.CompareString(text, "Sounds", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "HUD", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Scripts", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Cometary", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Talent", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Helmets", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Nations", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "Showroom", false) == 0)
        this.cbCat.Items.Add((object) "Unknown");
      else if (Operators.CompareString(text, "UI", false) == 0)
      {
        this.cbCat.Items.Add((object) "Unknown");
      }
      else
      {
        if (Operators.CompareString(text, "Other", false) != 0)
          return;
        this.cbCat.Items.Add((object) "Unknown");
      }
    }

    private void openCmpFile()
    {
      this.masFilesCol.Clear();
      Form1.cmpInfoFile cmpInfoFile = new Form1.cmpInfoFile(this.cmpInfoPath);
      this.tbAuthor.Text = cmpInfoFile.author;
      this.tbDate.Text = Operators.CompareString(cmpInfoFile.cmpDate, "", false) == 0 ? Conversions.ToString(DateTime.Now) : Conversions.ToString(DateTime.FromFileTimeUtc(Conversions.ToLong(cmpInfoFile.cmpDate)));
      if (Operators.CompareString(cmpInfoFile.desc, "", false) != 0)
        this.tbDesc.Text = cmpInfoFile.desc.Remove(checked (cmpInfoFile.desc.Length - 1), 1).Replace("\x000F", "\r\n");
      this.tbName.Text = cmpInfoFile.name;
      this.tbURL.Text = cmpInfoFile.url;
      this.tbVer.Text = Operators.CompareString(this.cmdlineVersion, "", false) != 0 ? this.cmdlineVersion : cmpInfoFile.version;
      this.cbType.Text = Conversions.ToString(cmpInfoFile.TypeTypes[(object) cmpInfoFile.type]);
      this.cbOrig.Text = Conversions.ToString(cmpInfoFile.OriginTypes[(object) cmpInfoFile.origin]);
      this.cbCat.Text = Conversions.ToString(cmpInfoFile.CategoryTypes[(object) cmpInfoFile.catagory]);
      this.tbRfcmpPath.Text = Path.GetDirectoryName(cmpInfoFile.location);
      try
      {
        foreach (object masFile in cmpInfoFile.masFiles)
          this.masFilesCol.Add(RuntimeHelpers.GetObjectValue(RuntimeHelpers.GetObjectValue(masFile)), (string) null, (object) null, (object) null);
      }
      finally
      {
      }
      this.lMasCount.Text = "MAS Files: " + Conversions.ToString(this.masFilesCol.Count);
    }

    private void bOpen_Click(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "Component Info Files (*.cmpinfo, *.mft)|*.cmpinfo;*.mft";
      openFileDialog.FilterIndex = 0;
      openFileDialog.Multiselect = false;
      int num = (int) openFileDialog.ShowDialog();
      if (Operators.CompareString(openFileDialog.FileName, "", false) == 0)
        return;
      this.cmpInfoPath = openFileDialog.FileName;
      this.openCmpFile();
    }

    private void bBrowse_Click(object sender, EventArgs e)
    {
      FolderSelectDialog folderSelectDialog = new FolderSelectDialog();
      folderSelectDialog.InitialDirectory = this.tbRfcmpPath.Text;
      folderSelectDialog.ShowDialog();
      if (Operators.CompareString(folderSelectDialog.FileName, "", false) == 0)
        return;
      if (this.relativePaths)
        this.tbRfcmpPath.Text = this.makeRelative(Environment.CurrentDirectory, folderSelectDialog.FileName);
      else
        this.tbRfcmpPath.Text = folderSelectDialog.FileName;
    }

    private void saveCmpInfoFile(string _cmpInfoPath)
    {
      if (Operators.CompareString(_cmpInfoPath, "", false) == 0)
      {
        SaveFileDialog saveFileDialog = new SaveFileDialog();
        saveFileDialog.Filter = "Component Info Files (*.cmpinfo)|*.cmpinfo";
        saveFileDialog.FilterIndex = 0;
        int num = (int) saveFileDialog.ShowDialog();
        if (Operators.CompareString(saveFileDialog.FileName, "", false) == 0)
          return;
        _cmpInfoPath = saveFileDialog.FileName;
      }
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine("[Component]");
      stringBuilder.AppendLine("Name=" + this.tbName.Text);
      stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Type=", this.TypeTypes[(object) this.cbType.Text])));
      stringBuilder.AppendLine("Version=" + this.tbVer.Text);
      stringBuilder.AppendLine("BaseVersion=");
      stringBuilder.AppendLine("MinVersion=");
      stringBuilder.AppendLine("Author=" + this.tbAuthor.Text);
      if (this.cmpInfoPath.ToLower().Trim().EndsWith(".mft"))
      {
        if (Operators.CompareString(this.tbDate.Text, "", false) == 0)
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
        else
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Parse(this.tbDate.Text).ToFileTimeUtc()));
      }
      else
        stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
      stringBuilder.AppendLine("ID=");
      stringBuilder.AppendLine("URL=" + this.tbURL.Text);
      stringBuilder.AppendLine("Desc=" + this.tbDesc.Text.Replace("\r\n", "\x000F") + "\x000F");
      if (Operators.CompareString(this.cbType.Text, "Location", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.LocCategoryTypes[(object) this.cbCat.Text])));
      else if (Operators.CompareString(this.cbType.Text, "Vehicle", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.VehCategoryTypes[(object) this.cbCat.Text])));
      else
        stringBuilder.AppendLine("Category=0");
      if (Operators.CompareString(this.cbOrig.Text, "", false) == 0)
        stringBuilder.AppendLine("Origin=0");
      else
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Origin=", this.OriginTypes[(object) this.cbOrig.Text])));
      stringBuilder.AppendLine("Flags=0");
      stringBuilder.AppendLine("CurLocation=0");
      stringBuilder.AppendLine("NumLocations=1");
      stringBuilder.AppendLine("Location=" + this.tbRfcmpPath.Text + "\\" + this.tbName.Text.Replace(" ", "_") + "_v" + this.tbVer.Text + ".rfcmp");
      stringBuilder.AppendLine("NumMASFiles=" + Conversions.ToString(this.masFilesCol.Count));
      try
      {
        foreach (object obj in this.masFilesCol)
        {
          object objectValue = RuntimeHelpers.GetObjectValue(obj);
          stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "MASFile=", objectValue)));
        }
      }
      finally
      {
      }
      stringBuilder.AppendLine("rFmFile=");
      stringBuilder.AppendLine("IconFile=");
      stringBuilder.AppendLine("SmallIconFile=");
      Microsoft.VisualBasic.FileIO.FileSystem.WriteAllText(_cmpInfoPath, stringBuilder.ToString(), false);
    }

    private void bSave_Click(object sender, EventArgs e)
    {
      if (!(Operators.CompareString(Path.GetExtension(this.cmpInfoPath).ToLower(), ".cmpinfo", false) == 0 | Operators.CompareString(this.cmpInfoPath, "", false) == 0))
        return;
      this.saveCmpInfoFile(this.cmpInfoPath);
    }

    private void package()
    {
      if (Operators.CompareString(Path.GetExtension(this.cmpInfoPath).ToLower(), ".cmpinfo", false) == 0)
        this.saveCmpInfoFile(this.cmpInfoPath);
      try
      {
        foreach (object obj in this.masFilesCol)
        {
          if (!Microsoft.VisualBasic.FileIO.FileSystem.FileExists(Conversions.ToString(RuntimeHelpers.GetObjectValue(obj))))
          {
            int num = (int) Interaction.MsgBox((object) "One or more of the MAS files associated with this component no longer exist. Check your MAS files and try again.", MsgBoxStyle.OkOnly, (object) null);
            return;
          }
        }
      }
      finally
      {
      }
      string str = Environment.GetEnvironmentVariable("UserProfile") + "\\AppData\\Roaming\\cmpinfo.dat";
      if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists(str))
        Microsoft.VisualBasic.FileIO.FileSystem.MoveFile(str, str + ".temp", true);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine("[Component]");
      stringBuilder.AppendLine("Name=" + this.tbName.Text);
      stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Type=", this.TypeTypes[(object) this.cbType.Text])));
      stringBuilder.AppendLine("Version=" + this.tbVer.Text);
      stringBuilder.AppendLine("BaseVersion=");
      stringBuilder.AppendLine("MinVersion=");
      stringBuilder.AppendLine("Author=" + this.tbAuthor.Text);
      if (this.cmpInfoPath.ToLower().Trim().EndsWith(".mft"))
      {
        if (Operators.CompareString(this.tbDate.Text, "", false) == 0)
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
        else
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Parse(this.tbDate.Text).ToFileTimeUtc()));
      }
      else
        stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
      stringBuilder.AppendLine("ID=");
      stringBuilder.AppendLine("URL=" + this.tbURL.Text);
      stringBuilder.AppendLine("Desc=" + this.tbDesc.Text.Replace("\r\n", "\x000F") + "\x000F");
      if (Operators.CompareString(this.cbType.Text, "Location", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.LocCategoryTypes[(object) this.cbCat.Text])));
      else if (Operators.CompareString(this.cbType.Text, "Vehicle", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.VehCategoryTypes[(object) this.cbCat.Text])));
      else
        stringBuilder.AppendLine("Category=0");
      stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Origin=", this.OriginTypes[(object) this.cbOrig.Text])));
      stringBuilder.AppendLine("Flags=0");
      stringBuilder.AppendLine("CurLocation=0");
      stringBuilder.AppendLine("NumLocations=1");
      stringBuilder.AppendLine("Location=" + this.makeAbsPath(this.tbRfcmpPath.Text) + "\\" + this.tbName.Text.Replace(" ", "_") + "_v" + this.tbVer.Text + ".rfcmp");
      stringBuilder.AppendLine("NumMASFiles=" + Conversions.ToString(this.masFilesCol.Count));
      try
      {
        foreach (object obj in this.masFilesCol)
        {
          object objectValue = RuntimeHelpers.GetObjectValue(obj);
          stringBuilder.AppendLine("MASFile=" + this.makeAbsPath(Conversions.ToString(objectValue)));
        }
      }
      finally
      {
      }
      stringBuilder.AppendLine("rFmFile=");
      stringBuilder.AppendLine("IconFile=");
      stringBuilder.AppendLine("SmallIconFile=");
      stringBuilder.AppendLine("");
      stringBuilder.AppendLine("CurComponent=0");
      Microsoft.VisualBasic.FileIO.FileSystem.WriteAllText(str, stringBuilder.ToString(), false, Encoding.ASCII);
      Process process = new Process();
      process.StartInfo.FileName = Conversions.ToString(this.mas2Path);
      process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
      process.Start();
      while (process.MainWindowHandle == (IntPtr) 0)
        Thread.Sleep(10);
      this.mas2Window = this.GetAutomationElementFromProcessId(process.Id);
      new Thread((ParameterizedThreadStart) (a0 => this.clickButton(Conversions.ToString(a0)))).Start((object) "Create the package file.");
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Create Single Cmp Package")) == (AutomationElement) null)
        Thread.Sleep(100);
      new Thread((ParameterizedThreadStart) (a0 => this.clickButton(Conversions.ToString(a0)))).Start((object) "Create Single Cmp Package");
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Package")) == (AutomationElement) null)
        Thread.Sleep(100);
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Component packaging complete with no errors")) == (AutomationElement) null)
        Thread.Sleep(100);
      process.Kill();
      if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists(str + ".temp"))
        Microsoft.VisualBasic.FileIO.FileSystem.MoveFile(str + ".temp", str, true);
      if (!this.cmdlineBuild)
        return;
      this.Close();
    }

    private void fullPackage()
    {
      if (Operators.CompareString(Path.GetExtension(this.cmpInfoPath).ToLower(), ".cmpinfo", false) == 0)
        this.saveCmpInfoFile(this.cmpInfoPath);
      try
      {
        foreach (object obj in this.masFilesCol)
        {
          if (!Microsoft.VisualBasic.FileIO.FileSystem.FileExists(Conversions.ToString(RuntimeHelpers.GetObjectValue(obj))))
          {
            int num = (int) Interaction.MsgBox((object) "One or more of the MAS files associated with this component no longer exist. Check your MAS files and try again.", MsgBoxStyle.OkOnly, (object) null);
            return;
          }
        }
      }
      finally
      {
      }
      string str = Environment.GetEnvironmentVariable("UserProfile") + "\\AppData\\Roaming\\cmpinfo.dat";
      if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists(str))
        Microsoft.VisualBasic.FileIO.FileSystem.MoveFile(str, str + ".temp", true);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine("[Component]");
      stringBuilder.AppendLine("Name=" + this.tbName.Text);
      stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Type=", this.TypeTypes[(object) this.cbType.Text])));
      stringBuilder.AppendLine("Version=" + this.tbVer.Text);
      stringBuilder.AppendLine("BaseVersion=");
      stringBuilder.AppendLine("MinVersion=");
      stringBuilder.AppendLine("Author=" + this.tbAuthor.Text);
      if (this.cmpInfoPath.ToLower().Trim().EndsWith(".mft"))
      {
        if (Operators.CompareString(this.tbDate.Text, "", false) == 0)
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
        else
          stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Parse(this.tbDate.Text).ToFileTimeUtc()));
      }
      else
        stringBuilder.AppendLine("Date=" + Conversions.ToString(DateTime.Now.ToFileTimeUtc()));
      stringBuilder.AppendLine("ID=");
      stringBuilder.AppendLine("URL=" + this.tbURL.Text);
      stringBuilder.AppendLine("Desc=" + this.tbDesc.Text.Replace("\r\n", "\x000F") + "\x000F");
      if (Operators.CompareString(this.cbType.Text, "Location", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.LocCategoryTypes[(object) this.cbCat.Text])));
      else if (Operators.CompareString(this.cbType.Text, "Vehicle", false) == 0)
        stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Category=", this.VehCategoryTypes[(object) this.cbCat.Text])));
      else
        stringBuilder.AppendLine("Category=0");
      stringBuilder.AppendLine(Conversions.ToString(Operators.ConcatenateObject((object) "Origin=", this.OriginTypes[(object) this.cbOrig.Text])));
      stringBuilder.AppendLine("Flags=0");
      stringBuilder.AppendLine("CurLocation=0");
      stringBuilder.AppendLine("NumLocations=1");
      stringBuilder.AppendLine("Location=" + this.makeAbsPath(this.tbRfcmpPath.Text) + "\\" + this.tbName.Text.Replace(" ", "_") + "_v" + this.tbVer.Text + ".rfcmp");
      stringBuilder.AppendLine("NumMASFiles=" + Conversions.ToString(this.masFilesCol.Count));
      try
      {
        foreach (object obj in this.masFilesCol)
        {
          object objectValue = RuntimeHelpers.GetObjectValue(obj);
          stringBuilder.AppendLine("MASFile=" + this.makeAbsPath(Conversions.ToString(objectValue)));
        }
      }
      finally
      {
      }
      stringBuilder.AppendLine("rFmFile=");
      stringBuilder.AppendLine("IconFile=");
      stringBuilder.AppendLine("SmallIconFile=");
      stringBuilder.AppendLine("");
      stringBuilder.AppendLine("CurComponent=0");
      Microsoft.VisualBasic.FileIO.FileSystem.WriteAllText(str, stringBuilder.ToString(), false, Encoding.ASCII);
      Process process = new Process();
      process.StartInfo.FileName = Conversions.ToString(this.mas2Path);
      process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
      process.Start();
      while (process.MainWindowHandle == (IntPtr) 0)
        Thread.Sleep(10);
      this.mas2Window = this.GetAutomationElementFromProcessId(process.Id);
      new Thread((ParameterizedThreadStart) (a0 => this.clickButton(Conversions.ToString(a0)))).Start((object) "Create the package file.");
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Create Single Cmp Package")) == (AutomationElement) null)
        Thread.Sleep(100);
      new Thread((ParameterizedThreadStart) (a0 => this.clickButton(Conversions.ToString(a0)))).Start((object) "Create Single Cmp Package");
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Package")) == (AutomationElement) null)
        Thread.Sleep(100);
      this.clickButton("Package");
      while (this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) "Component packaging complete with no errors")) == (AutomationElement) null)
        Thread.Sleep(100);
      process.Kill();
      if (Microsoft.VisualBasic.FileIO.FileSystem.FileExists(str + ".temp"))
        Microsoft.VisualBasic.FileIO.FileSystem.MoveFile(str + ".temp", str, true);
      if (!this.cmdlineBuild)
        return;
      this.Close();
    }

    private void bPackage_Click(object sender, EventArgs e)
    {
      if (Keyboard.IsKeyDown(Key.LeftCtrl) | Keyboard.IsKeyDown(Key.RightCtrl))
      {
        this.package();
      }
      else
      {
        if (Keyboard.IsKeyDown(Key.LeftCtrl) | Keyboard.IsKeyDown(Key.RightCtrl))
          return;
        this.fullPackage();
      }
    }

    private void clickButton(string buttonName)
    {
      ((InvokePattern) this.mas2Window.FindFirst(TreeScope.Descendants, (Condition) new PropertyCondition(AutomationElement.NameProperty, (object) buttonName)).GetCurrentPattern(InvokePattern.Pattern)).Invoke();
    }

    public AutomationElementCollection FindByMultipleConditions(AutomationElement elementWindowElement)
    {
      if ((object) elementWindowElement == null)
        throw new ArgumentException();
      AndCondition andCondition = new AndCondition(new Condition[2]
      {
        (Condition) new PropertyCondition(AutomationElement.IsEnabledProperty, (object) true),
        (Condition) new PropertyCondition(AutomationElement.ControlTypeProperty, (object) ControlType.Button)
      });
      return elementWindowElement.FindAll(TreeScope.Children, (Condition) andCondition);
    }

    private AutomationElement GetAutomationElementFromProcessId(int processId)
    {
      AutomationElement element = TreeWalker.RawViewWalker.GetFirstChild(AutomationElement.RootElement);
      while ((object) element != null && element.Current.ProcessId != processId)
        element = TreeWalker.RawViewWalker.GetNextSibling(element);
      return element;
    }

    private void Button1_Click(object sender, EventArgs e)
    {
      MyProject.Forms.dSettings.Show();
    }

    private void bDefAuth_Click(object sender, EventArgs e)
    {
      this.tbAuthor.Text = this.defAuthor;
    }

    private void bDefURL_Click(object sender, EventArgs e)
    {
      this.tbURL.Text = this.defURL;
    }

    private void bNew_Click(object sender, EventArgs e)
    {
      this.cmpInfoPath = "";
      this.masFilesCol.Clear();
      this.lMasCount.Text = "MAS Files: 0";
      this.tbAuthor.Clear();
      this.tbDate.Clear();
      this.tbDesc.Clear();
      this.tbName.Clear();
      this.tbRfcmpPath.Text = this.defOutPath;
      this.tbURL.Clear();
      this.tbVer.Clear();
    }

    private void bSaveAs_Click(object sender, EventArgs e)
    {
      this.saveCmpInfoFile("");
    }

    public string tryRelative(string basePath, string absPath)
    {
      if (absPath.ToLower().Contains(basePath.ToLower()))
        return Regex.Replace(absPath, Regex.Escape(basePath), ".", RegexOptions.IgnoreCase);
      return absPath;
    }

    public string makeAbsPath(string relPath)
    {
      return Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, relPath));
    }

    public string makeRelative(string basePath, string absPath)
    {
      int length1 = basePath.Split('\\').Length;
      int length2 = absPath.Split('\\').Length;
      string str = "";
      int num1 = 0;
      int num2 = checked (length1 - 1);
      int index1 = num1;
      while (index1 <= num2)
      {
        if (Operators.CompareString(basePath.Split('\\')[index1].ToLower(), absPath.Split('\\')[index1].ToLower(), false) == 0)
          checked { ++index1; }
        else
          break;
      }
      if (index1 == length1)
        return ".\\";
      int num3 = 1;
      int num4 = checked (length1 - index1);
      int num5 = num3;
      while (num5 <= num4)
      {
        str += "..\\";
        checked { ++num5; }
      }
      int num6 = index1;
      int num7 = checked (index1 + (length2 - index1) - 1);
      int index2 = num6;
      while (index2 <= num7)
      {
        str = str + absPath.Split('\\')[index2] + "\\";
        checked { ++index2; }
      }
      return str.TrimEnd('\\');
    }

    private void bNow_Click(object sender, EventArgs e)
    {
      this.tbDate.Text = Conversions.ToString(DateTime.Now);
    }

    public class cmpInfoFile
    {
      public string cmpInfoPath;
      public string name;
      public string version;
      public int type;
      public string author;
      public int origin;
      public int catagory;
      public string id;
      public string url;
      public string desc;
      public string cmpDate;
      public string flags;
      public int refCount;
      public string signature;
      public string location;
      public Collection masFiles;
      public Hashtable OriginTypes;
      public Hashtable CategoryTypes;
      public Hashtable TypeTypes;

      public cmpInfoFile(string cmpInfoPath)
      {
        this.type = 0;
        this.origin = 0;
        this.catagory = 0;
        this.masFiles = new Collection();
        this.OriginTypes = new Hashtable();
        this.CategoryTypes = new Hashtable();
        this.TypeTypes = new Hashtable();
        this.OriginTypes.Add((object) 0, (object) "Unknown");
        this.OriginTypes.Add((object) 1, (object) "Scratch");
        this.OriginTypes.Add((object) 2, (object) "Conversion");
        this.OriginTypes.Add((object) 3, (object) "Composite");
        this.OriginTypes.Add((object) 79, (object) "Unknown");
        this.CategoryTypes.Add((object) 0, (object) "Unknown");
        this.CategoryTypes.Add((object) 1, (object) "Dirt Oval");
        this.CategoryTypes.Add((object) 2, (object) "Dirt Raly");
        this.CategoryTypes.Add((object) 3, (object) "GT");
        this.CategoryTypes.Add((object) 4, (object) "Karts");
        this.CategoryTypes.Add((object) 5, (object) "Novel");
        this.CategoryTypes.Add((object) 6, (object) "Open Wheeler");
        this.CategoryTypes.Add((object) 7, (object) "Prototype");
        this.CategoryTypes.Add((object) 8, (object) "Stock Car");
        this.CategoryTypes.Add((object) 9, (object) "Street Car");
        this.CategoryTypes.Add((object) 10, (object) "Touring Car");
        this.CategoryTypes.Add((object) 50, (object) "Dirt Oval");
        this.CategoryTypes.Add((object) 51, (object) "Dirt Track");
        this.CategoryTypes.Add((object) 52, (object) "Kart Track");
        this.CategoryTypes.Add((object) 53, (object) "Novel");
        this.CategoryTypes.Add((object) 54, (object) "Oval");
        this.CategoryTypes.Add((object) 55, (object) "Permanent");
        this.CategoryTypes.Add((object) 56, (object) "Raly Cross");
        this.CategoryTypes.Add((object) 57, (object) "Temporary");
        this.TypeTypes.Add((object) 1, (object) "Location");
        this.TypeTypes.Add((object) 2, (object) "Vehicle");
        this.TypeTypes.Add((object) 3, (object) "Sounds");
        this.TypeTypes.Add((object) 4, (object) "HUD");
        this.TypeTypes.Add((object) 5, (object) "Scripts");
        this.TypeTypes.Add((object) 6, (object) "Cometary");
        this.TypeTypes.Add((object) 7, (object) "Talent");
        this.TypeTypes.Add((object) 8, (object) "Helmets");
        this.TypeTypes.Add((object) 9, (object) "Nations");
        this.TypeTypes.Add((object) 10, (object) "Showroom");
        this.TypeTypes.Add((object) 11, (object) "UI");
        this.TypeTypes.Add((object) 12, (object) "Other");
        this.cmpInfoPath = cmpInfoPath;
        string[] strArray = Regex.Split(Microsoft.VisualBasic.FileIO.FileSystem.ReadAllText(cmpInfoPath), "\r\n");
        int index = 0;
        while (index < strArray.Length)
        {
          string str1 = strArray[index];
          if (str1.Contains("="))
          {
            string str2 = str1.Split('=')[0];
            string Left = str1.Split('=')[1];
            string lower1 = str2.ToLower();
            if (Operators.CompareString(lower1, "name", false) == 0)
              this.name = Left;
            else if (Operators.CompareString(lower1, "version", false) == 0)
              this.version = Left;
            else if (Operators.CompareString(lower1, "type", false) == 0)
            {
              if (Operators.CompareString(Left, "", false) == 0)
                Left = Conversions.ToString(0);
              this.type = Conversions.ToInteger(Left);
            }
            else if (Operators.CompareString(lower1, "author", false) == 0)
              this.author = Left;
            else if (Operators.CompareString(lower1, "origin", false) == 0)
            {
              if (Operators.CompareString(Left, "", false) == 0)
                Left = Conversions.ToString(0);
              this.origin = Conversions.ToInteger(Left);
            }
            else if (Operators.CompareString(lower1, "category", false) == 0)
            {
              if (Operators.CompareString(Left, "", false) == 0)
                Left = Conversions.ToString(0);
              this.catagory = Conversions.ToInteger(Left);
            }
            else if (Operators.CompareString(lower1, "id", false) == 0)
              this.id = Left;
            else if (Operators.CompareString(lower1, "url", false) == 0)
              this.url = Left;
            else if (Operators.CompareString(lower1, "desc", false) == 0)
              this.desc = Left;
            else if (Operators.CompareString(lower1, "date", false) == 0)
              this.cmpDate = Left;
            else if (Operators.CompareString(lower1, "flags", false) == 0)
              this.flags = Left;
            else if (Operators.CompareString(lower1, "refcount", false) == 0)
              this.refCount = Conversions.ToInteger(Left);
            else if (Operators.CompareString(lower1, "signature", false) == 0)
              this.signature = Left;
            else if (Operators.CompareString(lower1, "masfile", false) == 0)
            {
              string lower2 = Path.GetExtension(cmpInfoPath).ToLower();
              if (Operators.CompareString(lower2, ".cmpinfo", false) == 0)
                this.masFiles.Add((object) Left, (string) null, (object) null, (object) null);
              else if (Operators.CompareString(lower2, ".mft", false) == 0)
                this.masFiles.Add((object) (Path.GetDirectoryName(cmpInfoPath) + "\\" + Left.Split(' ')[0]), (string) null, (object) null, (object) null);
            }
            else if (Operators.CompareString(lower1, "location", false) == 0)
              this.location = Left;
          }
          checked { ++index; }
        }
        if (Operators.CompareString(Path.GetExtension(cmpInfoPath).ToLower(), ".mft", false) != 0)
          return;
        this.location = Path.GetDirectoryName(cmpInfoPath) + "\\" + this.name.Replace(" ", "_") + "_v" + this.version + ".rfcmp";
      }
    }
  }
}
